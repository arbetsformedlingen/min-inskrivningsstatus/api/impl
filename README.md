# Min Inskrivningsstatus API

Returns the current registration status at Arbetsförmedlingen for the requested swedish personal number.

When calling the API the HTTP Accept header **MUST** be set to either *application/json* **OR** *text/plain*.

Example response for a person currently being registered:

*application/json*
<pre>
{
  "personalNumber":"200210272399"
  "registered":true
  "registrationDate":"2023-02-07"
  "deregistrationDate":null
}
</pre>

*text/plain*
<pre>
Inskriven, registrerad 2023-02-07
</pre>

Example response for a person not being registered:

*application/json*
<pre>
{
  "personalNumber":"200103142386"
  "registered":false,
  "registrationDate":null
  "deregistrationDate":null
}
</pre>

*text/plain*
<pre>
Ej inskriven
</pre>

Example response for a person that have been deregistered:

*application/json*
<pre>
{
  "personalNumber":"200103142386"
  "registered":false,
  "registrationDate":"2023-02-07"
  "deregistrationDate":"2023-08-27"
}
</pre>

*text/plain*
<pre>
Ej inskriven, registrerad 2023-02-07, avregistrerad 2023-08-27
</pre>


For requests where the registration date is before 2022-12-01 an error response 451 (*Unavailable For Legal Reasons*) will be returned.