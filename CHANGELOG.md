# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [2.0.1](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/compare/v2.0.0...v2.0.1) (2025-03-04)


### Bug Fixes

* configure /health and /api-info endpoints to be publicly accessible ([0d308cb](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/0d308cbb36118b0f9dcc63dcb03ef33600f1d2a4))

## [2.0.0](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/compare/v1.5.1...v2.0.0) (2025-03-04)


### ⚠ BREAKING CHANGES

* implement api spec 2.0.0

### Features

* implement api spec 2.0.0 ([845ce3f](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/845ce3f8046fa650e16b3388c2b7e0f58f1590ac))


### Bug Fixes

* add necessary properties for ApiInfoController ([108b98e](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/108b98e65047910d4dbfa6fb021a2f3b083d8483))
* **deps:** update dependency com.fasterxml.jackson.datatype:jackson-datatype-jsr310 to v2.18.3 ([96b864d](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/96b864d63777d9c299abce7122e34b9bab194d0d))
* **deps:** update jackson monorepo to v2.18.3 ([a097eb8](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/a097eb8578c678ecb1c58afad726920917564a9d))

## [1.5.1](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/compare/v1.5.0...v1.5.1) (2025-02-28)


### Bug Fixes

* add jdk.management module to the Java runtime to enable CPU monitoring ([077dd62](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/077dd622e08c6d99f87bd4770b47c99f487132dd))

## [1.5.0](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/compare/v1.4.1...v1.5.0) (2025-02-28)


### Features

* add jdk.zipfs module needed by elastic apm agent ([c6fc530](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/c6fc530b6a63a0bce690e297bcdb47b1b1a40de4))


### Bug Fixes

* **deps:** update dependency jakarta.validation:jakarta.validation-api to v3.1.1 ([a97baeb](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/a97baeb87da7f41c78f67aac99b66fba6675dda2))
* **deps:** update spring boot to v3.4.3 ([17f412f](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/17f412ffd5ce546fb59131db212e8f97d82b78cc))
* remove registration date restriction ([66d6a94](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/66d6a9409a548831d1cfdb7b7f9c27ceac9c9e8e))

## [1.4.1](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/compare/v1.4.0...v1.4.1) (2025-01-31)


### Bug Fixes

* adopt rask spec v1.2.1 ([9441884](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/9441884bb7252dd9f6c82b3d88387dcb56d29bf9))
* **deps:** update dependency com.google.guava:guava to v33.4.0-jre ([4377b49](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/4377b495ae9b43217323574df79ef05d0b7e65cb))
* **deps:** update dependency io.swagger.core.v3:swagger-annotations-jakarta to v2.2.28 ([2450f2b](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/2450f2b95ff41abf3ad35b37f4e7a12f86c961d2))
* **deps:** update spring boot to v3.4.1 ([9efe855](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/9efe855b0fbae3cca49ad1d80c1d85982438cfa0))
* **deps:** update spring boot to v3.4.2 ([c2f37a4](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/c2f37a418dec9392d8f63957ae27a60eba0857b3))

## [1.4.0](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/compare/v1.3.2...v1.4.0) (2024-12-16)


### Features

* implement v1.0.0 of the Min Inskrivningsstatus specification ([97bb4b9](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/97bb4b9f08dca74ce4c849237e4eb8f43d8eb9f2))


### Bug Fixes

* add deregistrationDate to json response ([b1102ff](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/b1102ffb2f908ea8b7c877cb4390088fc5088e4d))
* add documentation in README ([2079a7e](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/2079a7e831f5c97889681a4cf2cc28b6909b160b))
* **deps:** update dependency io.swagger.core.v3:swagger-annotations-jakarta to v2.2.27 ([5c21d10](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/5c21d10b6694686b95e00c7110b60c07cb529961))

## [1.3.2](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/compare/v1.3.1...v1.3.2) (2024-12-10)


### Bug Fixes

* plain/text response should not return null ([d473889](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/d4738890ea9669b2a0ea10853d3bf0448bed8f53))

## [1.3.1](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/compare/v1.3.0...v1.3.1) (2024-12-10)


### Bug Fixes

* implement desired logic in the json and text formatting code ([7ed6792](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/7ed6792564762b7d4e933b41084887718ea92448))

## [1.3.0](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/compare/v1.2.0...v1.3.0) (2024-12-10)


### Features

* return registrationDate even if isRegistered is FALSE ([b9c4ef7](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/b9c4ef749e3afd366c14339770ac74cee62ab6df))


### Bug Fixes

* negative test ([1f2b5e4](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/1f2b5e4a6d06bab83d05bbb557f665f20694725f))

## [1.2.0](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/compare/v1.1.0...v1.2.0) (2024-12-09)


### Features

* add description to readme ([ab45804](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/ab4580470f2780b345b6783f8595699d61c44280))
* automatically determine context path by the api specification version ([c510e21](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/c510e21c74998c9c08e94d529229fd30cc2b6cf6))
* return isRegistered false for unregistered individuals ([200db95](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/200db95bd36f08269b01ec814a0a482ff2ef07d8))


### Bug Fixes

* **deps:** update dependency com.fasterxml.jackson.core:jackson-databind to v2.18.0 ([120e1b6](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/120e1b647d3bdc149a3f9adbea381a07655711b9))
* **deps:** update dependency com.fasterxml.jackson.core:jackson-databind to v2.18.1 ([712ed21](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/712ed21c85450906b1697185d7bd2285a0d594a1))
* **deps:** update dependency com.fasterxml.jackson.core:jackson-databind to v2.18.2 ([7a42086](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/7a42086ce5c63fe72eba24c744c748a4e6a318c2))
* **deps:** update dependency com.fasterxml.jackson.datatype:jackson-datatype-jsr310 to v2.17.2 ([842505e](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/842505e54d1fd604039fa89e8430be1303915b0b))
* **deps:** update dependency com.fasterxml.jackson.datatype:jackson-datatype-jsr310 to v2.18.0 ([03ae474](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/03ae4749949aa8e402cfc84f2eeec5302ce04f87))
* **deps:** update dependency com.fasterxml.jackson.datatype:jackson-datatype-jsr310 to v2.18.1 ([6995bc9](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/6995bc9bd55e56a65d2f54297facbff73d4d0093))
* **deps:** update dependency com.fasterxml.jackson.datatype:jackson-datatype-jsr310 to v2.18.2 ([eb83af0](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/eb83af021b79bf17e38340ac6cc6cfd11e0ae29c))
* **deps:** update dependency com.google.guava:guava to v33.3.0-jre ([d7174db](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/d7174db7898143ef117a9def8be1849bf7c7e8ba))
* **deps:** update dependency com.google.guava:guava to v33.3.1-jre ([2f04c94](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/2f04c9415e3052af372113ae902c9b8e0761bcaa))
* **deps:** update dependency io.swagger.core.v3:swagger-annotations-jakarta to v2.2.23 ([2cb9ab9](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/2cb9ab91ba62975956087ea149e2e90694fdea83))
* **deps:** update dependency io.swagger.core.v3:swagger-annotations-jakarta to v2.2.24 ([580d6e0](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/580d6e0526ba4019e6b764e1837f5f77d0396d8d))
* **deps:** update dependency io.swagger.core.v3:swagger-annotations-jakarta to v2.2.25 ([6e2c3f8](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/6e2c3f8c6dc185d7e9d5c1938f496e177dc4427d))
* **deps:** update dependency io.swagger.core.v3:swagger-annotations-jakarta to v2.2.26 ([b043aab](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/b043aabf952dc50caadb52905119aa21731d2a3d))
* **deps:** update dependency net.logstash.logback:logstash-logback-encoder to v8 ([ca89c25](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/ca89c251da050716d659c74e118cc292d980d1a6))
* **deps:** update dependency org.apache.httpcomponents.client5:httpclient5 to v5.3.1 ([3449952](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/344995229f841a04a2f350fef34137a29b6f7996))
* **deps:** update dependency org.apache.httpcomponents.client5:httpclient5 to v5.4 ([5600725](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/5600725b6f0d65d5ea13e50b8e9d293ce03476f2))
* **deps:** update dependency org.apache.httpcomponents.client5:httpclient5 to v5.4 ([d54db91](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/d54db91b4cd7283e0cfb0a4d6b1dd5e11863a0bc))
* **deps:** update dependency org.projectlombok:lombok to v1.18.34 ([8685872](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/8685872296525bf97408dfaf0f8ca231c63edf5d))
* **deps:** update dependency org.projectlombok:lombok to v1.18.36 ([22ad085](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/22ad08564c1505db0563af6d2cfd6378579d513b))
* **deps:** update dependency org.springframework:spring-web to v6.1.11 ([d075ace](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/d075ace913e165a84400cb04e7ed0c4810e96799))
* **deps:** update dependency org.springframework:spring-web to v6.1.12 ([10a21ae](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/10a21ae1670321bc1c5b5ddb841a2b7f21b49de5))
* **deps:** update dependency org.springframework:spring-web to v6.1.13 ([d052caa](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/d052caad589b3260887ea1fc125b1ac40d9006ec))
* **deps:** update dependency org.springframework:spring-web to v6.1.14 ([cf6e702](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/cf6e702f9b913e6574351c3c271693541f7056f8))
* **deps:** update jackson.version to v2.17.2 ([05eea11](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/05eea1128348ea5f542605c45b6c9d8d82e553e4))
* **deps:** update spring core to v6.2.0 ([10ceb33](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/10ceb3353571eb9771c42745bfd767a166079765))
* propagate api.spec.version from pom.xml to application.properties ([90282e0](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/90282e08fb5d0fada0edfdd045a7f7ae07b4a0b6))
* remove explicit version from httpclient5, add explicit dependency version to fixed vulnerable spring-context ([69a7696](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/69a769607475e0d0e55300944a5dc661099c1f13))
* removed faulty regex from logback json encoder ([8f5a5db](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/8f5a5db34b51874c511fd55fcdfee247924ef2a5))
* revert incompatible dependency version org.apache.httpscomponents.client5:httpclient5:5.4 back to 5.3.1 ([b29718b](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/b29718b0bbc276cdf0a6300ddefaaf00f4852299))

## [1.1.0](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/compare/v1.0.0...v1.1.0) (2024-06-24)


### Features

* add proxy support for rask http requests ([3deec14](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/3deec1401fe4c7001f828863c8dc363f112c72b0))


### Bug Fixes

* **deps:** update dependency org.springframework:spring-web to v6.1.10 ([cc17e67](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/cc17e67b8bdcb0bdac3ace13245f031e0b65ec58))

## [1.0.0](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/compare/v0.0.2...v1.0.0) (2024-06-17)


### Bug Fixes

* **deps:** update dependency com.fasterxml.jackson.datatype:jackson-datatype-jsr310 to v2.17.1 ([5ad45c8](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/5ad45c8c5aba3764ca2315dbc8f120a8c9077a0f))
* **deps:** update dependency com.google.guava:guava to v33.2.0-jre ([3428d85](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/3428d85b090599468a8bdc51b24c91444e6ca977))
* **deps:** update dependency com.google.guava:guava to v33.2.1-jre ([4eb175f](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/4eb175f0312fd2810891597512fd14f2eeb2d890))
* **deps:** update dependency io.swagger.core.v3:swagger-annotations-jakarta to v2.2.22 ([13fdf85](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/13fdf85d915fcf9ef6bada7335478692bc22b80f))
* **deps:** update dependency jakarta.validation:jakarta.validation-api to v3.1.0 ([81929b8](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/81929b8bb227ec722ce2c80ddc8a1929adc5db03))
* **deps:** update dependency org.springframework:spring-web to v6.1.7 ([eabed6c](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/eabed6c94fa9d7e29be9de70e0742a8b777eb2d8))
* **deps:** update dependency org.springframework:spring-web to v6.1.8 ([9e6493a](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/9e6493a7db73352407aaef882bf43359fde087d1))
* **deps:** update dependency org.springframework:spring-web to v6.1.9 ([f417ad7](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/f417ad740222360b66ea993167382ebb25b5feda))
* **deps:** update jackson.version to v2.17.1 ([db92a7d](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/db92a7daf467edeba5d061c241a846f6a215e4f3))

## 0.0.2 (2024-04-29)


### Features

* add rask client ([20d88fd](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/20d88fd5a938e902ecfef57fa903ba00fa5756d6))
* handle not before date ([527d638](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/527d6385a83361b976e597b364373049b7e53b05))
* initial ([642af4b](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/642af4bf581db3dea8e11d9a1c5bf241f6d32aa0))
* use rask client to fetch registration status ([0cf1cc4](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/0cf1cc4c0cbe82942581f7ec6c918f83f6a1385e))


### Bug Fixes

* align to figma board ([1d72c72](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/1d72c725c45d3e58026891af61d2265094648158))
* avoid usage of double wildcards in the security configuration ([8775b48](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/8775b48f0c237e24919595ea2b310efb0666ea5c))
* **deps:** update dependency com.fasterxml.jackson.datatype:jackson-datatype-jsr310 to v2.17.0 ([8ae804d](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/8ae804d749625f99b03660fc4e2b1c9f77a9cb4f))
* **deps:** update dependency com.google.guava:guava to v33.1.0-jre ([cbe4efc](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/cbe4efcfd795e9e8fd2213f2f1fb16638887357b))
* **deps:** update dependency io.swagger.core.v3:swagger-annotations-jakarta to v2.2.21 ([fcd2b7d](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/fcd2b7d020ba813fea497b245a15652bea3dc5db))
* **deps:** update dependency org.projectlombok:lombok to v1.18.32 ([4245fe5](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/4245fe5d63505443159d93b3aa69d44ba8ee2085))
* **deps:** update jackson.version to v2.17.0 ([4aad46f](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/4aad46f5b3bd6344b8941869317f97da0d4638c5))
* exclude generated Rask client from Jacoco coverage report ([5a032b4](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/5a032b4cae4eacfaf3faf83b29c7fc3cf7697048))
* failing test ([24779c3](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/24779c3256a87df4153056ab1dbf3ec8b3ce39f8))
* security pin org.springframework:spring-web:6.1.6 ([f57931e](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/f57931e00963cb9acc8d29937233acc384a6a21a))
* use spec v0.1.2 ([adeabdb](https://gitlab.com/arbetsformedlingen/min-inskrivningsstatus/api/impl/commit/adeabdbf585274c4c75a7d9330190124f041ed13))
