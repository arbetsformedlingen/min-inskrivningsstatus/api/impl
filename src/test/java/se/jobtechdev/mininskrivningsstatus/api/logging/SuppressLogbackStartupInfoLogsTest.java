package se.jobtechdev.mininskrivningsstatus.api.logging;

import ch.qos.logback.core.Context;
import ch.qos.logback.core.status.*;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SuppressLogbackStartupInfoLogsTest {
    static class TestSuppressLogbackStartupInfoLogsTest extends SuppressLogbackStartupInfoLogs {
        @Override
        protected void hookPostAddStatusEvent(Status status) {
            throw new RuntimeException("Throw here");
        }
    }

    @Test
    public void addStatusEvent_onError() {
        // Arrange
        final Status mockStatus = mock(Status.class);
        when(mockStatus.getLevel()).thenReturn(Status.ERROR);

        // Act
        final SuppressLogbackStartupInfoLogs suppressLogbackStartupInfoLogs = new TestSuppressLogbackStartupInfoLogsTest();
        final RuntimeException runtimeException = assertThrows(RuntimeException.class, () -> suppressLogbackStartupInfoLogs.addStatusEvent(mockStatus));

        // Assert
        assertEquals("Throw here", runtimeException.getMessage());
    }

    @Test
    public void addStatusEvent_onWarn() {
        // Arrange
        final Status mockStatus = mock(Status.class);
        when(mockStatus.getLevel()).thenReturn(Status.WARN);

        // Act
        final SuppressLogbackStartupInfoLogs suppressLogbackStartupInfoLogs = new TestSuppressLogbackStartupInfoLogsTest();
        final RuntimeException runtimeException = assertThrows(RuntimeException.class, () -> suppressLogbackStartupInfoLogs.addStatusEvent(mockStatus));

        // Assert
        assertEquals("Throw here", runtimeException.getMessage());
    }

    @Test
    public void addStatusEvent_onInfo_earlyExit() {
        // Arrange
        final Status mockStatus = mock(Status.class);
        when(mockStatus.getLevel()).thenReturn(Status.INFO);

        // Act
        final SuppressLogbackStartupInfoLogs suppressLogbackStartupInfoLogs = new TestSuppressLogbackStartupInfoLogsTest();
        try {
            suppressLogbackStartupInfoLogs.addStatusEvent(mockStatus);
        } catch (RuntimeException e) {
            // Assert
            fail();
        }
    }

    static class ThrowOnErrorLevelStartUp extends SuppressLogbackStartupInfoLogs {
        @Override
        protected void hookPostStart(Status status) {
            if (status.getLevel() == Status.ERROR)
                throw new RuntimeException("Throw here, status: " + status.getLevel());
        }
    }

    @Test
    public void start_onErrorLevelStartUp_exitEarlyWithViaHookToVerifyExecutionPath() {
        // Arrange
        final Status mockStatus = mock(Status.class);
        when(mockStatus.getLevel()).thenReturn(Status.ERROR);

        final Context mockContext = mock(Context.class);
        final StatusManager mockStatusManager = mock(StatusManager.class);
        final Status infoStatus = new InfoStatus("info", null);
        final Status warnStatus = new WarnStatus("warn", null);
        final Status errorStatus = new ErrorStatus("error", null);
        final List<Status> statuses = List.of(infoStatus, warnStatus, errorStatus);
        when(mockStatusManager.getCopyOfStatusList()).thenReturn(statuses);
        when(mockContext.getStatusManager()).thenReturn(mockStatusManager);

        // Act
        final SuppressLogbackStartupInfoLogs suppressLogbackStartupInfoLogs = new ThrowOnErrorLevelStartUp();
        suppressLogbackStartupInfoLogs.setContext(mockContext);
        final RuntimeException runtimeException = assertThrows(RuntimeException.class, suppressLogbackStartupInfoLogs::start);

        // Assert
        assertEquals(String.format("Throw here, status: %d", Status.ERROR), runtimeException.getMessage());
    }

    static class ThrowOnWarnLevelStartUp extends SuppressLogbackStartupInfoLogs {
        @Override
        protected void hookPostStart(Status status) {
            if (status.getLevel() == Status.WARN)
                throw new RuntimeException("Throw here, status: " + status.getLevel());
        }
    }

    @Test
    public void start_onWarnLevelStartUp_exitEarlyWithViaHookToVerifyExecutionPath() {
        // Arrange
        final Status mockStatus = mock(Status.class);
        when(mockStatus.getLevel()).thenReturn(Status.WARN);

        final Context mockContext = mock(Context.class);
        final StatusManager mockStatusManager = mock(StatusManager.class);
        final Status infoStatus = new InfoStatus("info", null);
        final Status warnStatus = new WarnStatus("warn", null);
        final Status errorStatus = new ErrorStatus("error", null);
        final List<Status> statuses = List.of(infoStatus, warnStatus, errorStatus);
        when(mockStatusManager.getCopyOfStatusList()).thenReturn(statuses);
        when(mockContext.getStatusManager()).thenReturn(mockStatusManager);

        // Act
        final SuppressLogbackStartupInfoLogs suppressLogbackStartupInfoLogs = new ThrowOnWarnLevelStartUp();
        suppressLogbackStartupInfoLogs.setContext(mockContext);
        final RuntimeException runtimeException = assertThrows(RuntimeException.class, suppressLogbackStartupInfoLogs::start);

        // Assert
        assertEquals(String.format("Throw here, status: %d", Status.WARN), runtimeException.getMessage());
    }

    static class ThrowOnInfoLevelStartUp extends SuppressLogbackStartupInfoLogs {
        @Override
        protected void hookPostStart(Status status) {
            if (status.getLevel() == Status.INFO)
                throw new RuntimeException("Throw here, status: " + status.getLevel());
        }
    }

    @Test
    public void start_onInfoLevelStartUp_expectNothingToBeThrown() {
        // Arrange
        final Status mockStatus = mock(Status.class);
        when(mockStatus.getLevel()).thenReturn(Status.WARN);

        final Context mockContext = mock(Context.class);
        final StatusManager mockStatusManager = mock(StatusManager.class);
        final Status infoStatus = new InfoStatus("info", null);
        final Status warnStatus = new WarnStatus("warn", null);
        final Status errorStatus = new ErrorStatus("error", null);
        final List<Status> statuses = List.of(infoStatus, warnStatus, errorStatus);
        when(mockStatusManager.getCopyOfStatusList()).thenReturn(statuses);
        when(mockContext.getStatusManager()).thenReturn(mockStatusManager);

        // Act
        final SuppressLogbackStartupInfoLogs suppressLogbackStartupInfoLogs = new ThrowOnInfoLevelStartUp();
        suppressLogbackStartupInfoLogs.setContext(mockContext);

        try {
            suppressLogbackStartupInfoLogs.start();
        } catch (RuntimeException e) {
            fail();
        }
    }

    @Test
    public void start() {
        // Arrange
        final Status mockStatus = mock(Status.class);
        when(mockStatus.getLevel()).thenReturn(Status.WARN);

        final Context mockContext = mock(Context.class);
        final StatusManager mockStatusManager = mock(StatusManager.class);
        final List<Status> statuses = List.of(new WarnStatus("warn", null));
        when(mockStatusManager.getCopyOfStatusList()).thenReturn(statuses);
        when(mockContext.getStatusManager()).thenReturn(mockStatusManager);

        // Act
        final SuppressLogbackStartupInfoLogs suppressLogbackStartupInfoLogs = new SuppressLogbackStartupInfoLogs();
        suppressLogbackStartupInfoLogs.setContext(mockContext);

        try {
            suppressLogbackStartupInfoLogs.start();
        } catch (RuntimeException e) {
            fail();
        }
    }

    @Test
    public void hookPostAddStatusEvent() {
        final var mockStatus = mock(Status.class);
        when(mockStatus.getLevel()).thenReturn(Status.WARN);
        final var example = new SuppressLogbackStartupInfoLogs();
        example.addStatusEvent(mockStatus);
    }
}
