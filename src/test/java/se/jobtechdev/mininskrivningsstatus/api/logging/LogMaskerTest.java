package se.jobtechdev.mininskrivningsstatus.api.logging;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class LogMaskerTest {
    @Test
    @Tag("UnitTest")
    public void construct() {
        new LogMasker();
    }

    @Test
    @Tag("UnitTest")
    public void mask_whenThereIsNothingtoMask_returnTheOriginalString() {
        final var masked = LogMasker.mask("{\\\"foo\\\":\\\"bar\\\"}");
        assertEquals("{\\\"foo\\\":\\\"bar\\\"}", masked);
    }

    @Test
    @Tag("UnitTest")
    public void mask_whenEscapedPersonalIdentityNumberIsPresent_replaceWithRedactedText() {
        final var masked = LogMasker.mask("{\\\"personalIdentityNumber\\\":\\\"199010102383\\\"}");
        assertEquals("{\\\"personalIdentityNumber\\\":\\\"REDACTED\\\"}", masked);
    }

    @Test
    @Tag("UnitTest")
    public void mask_whenPersonalIdentityNumberIsPresent_replaceWithRedactedText() {
        final var masked = LogMasker.mask("{\"personalIdentityNumber\":\"199010102383\"}");
        assertEquals("{\"personalIdentityNumber\":\"REDACTED\"}", masked);
    }
}
