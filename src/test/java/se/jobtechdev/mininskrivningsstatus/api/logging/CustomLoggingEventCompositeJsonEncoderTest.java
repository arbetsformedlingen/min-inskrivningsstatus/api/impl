package se.jobtechdev.mininskrivningsstatus.api.logging;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class CustomLoggingEventCompositeJsonEncoderTest {
    class AuditTestCase extends CustomLoggingEventCompositeJsonEncoder {
        @Override
        protected byte[] hookPreMasking(byte[] bytes) {
            return "{\"audit\": {}}".getBytes(StandardCharsets.UTF_8);
        }
    }

    class NonAuditTestCase extends CustomLoggingEventCompositeJsonEncoder {
        @Override
        protected byte[] hookPreMasking(byte[] bytes) {
            return "{\"foo\": {}}".getBytes(StandardCharsets.UTF_8);
        }
    }

    @Test
    @Tag("UnitTest")
    public void encode_whenAuditIsPresent_returnRawMessage() {
        try (MockedStatic<LogMasker> logMaskerMockedStatic = mockStatic(LogMasker.class)) {
            final ILoggingEvent event = mock(ILoggingEvent.class);
            when(event.getFormattedMessage()).thenReturn("example-formatted-message");
            when(event.getMessage()).thenReturn("example-message");

            when(event.getLevel()).thenReturn(Level.ALL);
            logMaskerMockedStatic.when(() -> LogMasker.mask("{\"audit\": {}}")).thenReturn("masked");

            final CustomLoggingEventCompositeJsonEncoder encoder = new AuditTestCase();
            try {
                encoder.start();
                final byte[] bytes = encoder.encode(event);

                logMaskerMockedStatic.verify(() -> LogMasker.mask(anyString()), times(0));
                assertEquals("{\"audit\": {}}", new String(bytes));
            } finally {
                encoder.stop();
            }
        }
    }

    @Test
    @Tag("UnitTest")
    public void encode_whenAuditIsNotPresent_returnMaskedMessage() {
        try (MockedStatic<LogMasker> logMaskerMockedStatic = mockStatic(LogMasker.class)) {
            final ILoggingEvent event = mock(ILoggingEvent.class);
            when(event.getFormattedMessage()).thenReturn("example-formatted-message");
            when(event.getMessage()).thenReturn("example-message");

            when(event.getLevel()).thenReturn(Level.ALL);
            logMaskerMockedStatic.when(() -> LogMasker.mask("{\"foo\": {}}")).thenReturn("masked");

            final CustomLoggingEventCompositeJsonEncoder encoder = new NonAuditTestCase();
            try {
                encoder.start();
                final byte[] bytes = encoder.encode(event);

                logMaskerMockedStatic.verify(() -> LogMasker.mask("{\"foo\": {}}"), times(1));
                assertEquals("masked", new String(bytes));
            } finally {
                encoder.stop();
            }
        }
    }

    @Test
    @Tag("UnitTest")
    public void hookPreMasking() {
        final CustomLoggingEventCompositeJsonEncoder encoder = new CustomLoggingEventCompositeJsonEncoder();
        byte[] bytes = new byte[]{};
        assertEquals(bytes, encoder.hookPreMasking(bytes));
    }
}
