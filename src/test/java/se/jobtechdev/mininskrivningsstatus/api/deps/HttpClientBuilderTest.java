package se.jobtechdev.mininskrivningsstatus.api.deps;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

public class HttpClientBuilderTest {
  @Test
  @Tag("Integration")
  public void buildHttpClient() {
    assertDoesNotThrow(() -> HttpClients.custom().build());
  }
}
