package se.jobtechdev.mininskrivningsstatus.api.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.Test;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.CorsRegistration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class WebMvcConfigTest {
    @Test
    public void addCorsMappings_with_extraAllowedOrigins() {
        // Arrange
        final var mockCorsRegistry = mock(CorsRegistry.class);
        final var mockCorsRegistration = mock(CorsRegistration.class);
        when(mockCorsRegistry.addMapping("/**")).thenReturn(mockCorsRegistration);
        when(mockCorsRegistration.allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")).thenReturn(mockCorsRegistration);
        when(mockCorsRegistration.allowedOrigins("http://example.com")).thenReturn(mockCorsRegistration);

        // Act
        final var corsConfig = new WebMvcConfig(new String[]{"http://example.com"});
        corsConfig.addCorsMappings(mockCorsRegistry);

        // Assert
        verify(mockCorsRegistry, times(1)).addMapping("/**");
        verify(mockCorsRegistration, times(1)).allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS");
        verify(mockCorsRegistration, times(1)).allowedOrigins("http://example.com");
    }

    @Test
    public void addCorsMappings_without_extraAllowedOrigins() {
        // Arrange
        final var mockCorsRegistry = mock(CorsRegistry.class);
        final var mockCorsRegistration = mock(CorsRegistration.class);
        when(mockCorsRegistry.addMapping("/**")).thenReturn(mockCorsRegistration);
        when(mockCorsRegistration.allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")).thenReturn(mockCorsRegistration);
        when(mockCorsRegistration.allowedOrigins("http://example.com")).thenReturn(mockCorsRegistration);

        // Act
        final var corsConfig = new WebMvcConfig(null);
        corsConfig.addCorsMappings(mockCorsRegistry);

        // Assert
        verify(mockCorsRegistry, times(0)).addMapping("/**");
        verify(mockCorsRegistration, times(0)).allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS");
        verify(mockCorsRegistration, times(0)).allowedOrigins("http://example.com");
    }

    @Test
    public void extendMessageConverters() {
        // Arrange
        try (
                final var constructionMappingJackson2HttpMessageConverter = mockConstruction(MappingJackson2HttpMessageConverter.class);
                final var constructionObjectMapper = mockConstruction(ObjectMapper.class);
                final var constructionJavaTimeModule = mockConstruction(JavaTimeModule.class)
        ) {
            final var mockHttpMessageConverter = mock(HttpMessageConverter.class);
            List<HttpMessageConverter<?>> converters = new ArrayList<>();
            converters.add(mockHttpMessageConverter);

            // Act
            final var webMvcConfig = new WebMvcConfig(null);
            webMvcConfig.extendMessageConverters(converters);

            // Assert
            assertEquals(1, constructionMappingJackson2HttpMessageConverter.constructed().size());
            final var mockMappingJackson2HttpMessageConverter = constructionMappingJackson2HttpMessageConverter.constructed().get(0);
            assertEquals(1, constructionObjectMapper.constructed().size());
            final var mockObjectMapper = constructionObjectMapper.constructed().get(0);
            assertEquals(1, constructionJavaTimeModule.constructed().size());
            final var mockJavaTimeModule = constructionJavaTimeModule.constructed().get(0);
            verify(mockObjectMapper, times(1)).registerModule(mockJavaTimeModule);
            verify(mockObjectMapper, times(1)).configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
            verify(mockObjectMapper, times(1)).setSerializationInclusion(JsonInclude.Include.NON_NULL);
            verify(mockMappingJackson2HttpMessageConverter, times(1)).setObjectMapper(mockObjectMapper);
        }
    }
}
