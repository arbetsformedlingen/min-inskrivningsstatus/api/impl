package se.jobtechdev.mininskrivningsstatus.api.config;

import org.junit.jupiter.api.Test;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class ContextPathConfigTest {
  @Test
  public void
  webServerFactoryCustomizer_whenServerServletContextPathIsEmpty_returnMajorOfApiSpecVersion() {
    final var factoryMock = mock(ConfigurableServletWebServerFactory.class);
    final var contextPathConfig = new ContextPathConfig();

    final var lambda = contextPathConfig.webServerFactoryCustomizer("0.0.1", "");
    lambda.customize(factoryMock);

    verify(factoryMock, times(1)).setContextPath("/v0");
  }

  @Test
  public void
  webServerFactoryCustomizer_whenServerServletContextPathIsNotEmpty_returnServerServletContextPath() {
    final var factoryMock = mock(ConfigurableServletWebServerFactory.class);
    final var contextPathConfig = new ContextPathConfig();

    final var lambda = contextPathConfig.webServerFactoryCustomizer("0.0.1", "v123");
    lambda.customize(factoryMock);

    verify(factoryMock, times(1)).setContextPath("v123");
  }

  @Test
  public void webServerFactoryCustomizer_whenApiSpecVersionNotValidSemVer_thenThrowRuntimeException() {
    final var thrown = assertThrows(RuntimeException.class, () -> {
      new ContextPathConfig().webServerFactoryCustomizer("invalidSemanticVersion", null);
    });

    assertEquals("Invalid SemVer string: invalidSemanticVersion", thrown.getMessage());
  }
}
