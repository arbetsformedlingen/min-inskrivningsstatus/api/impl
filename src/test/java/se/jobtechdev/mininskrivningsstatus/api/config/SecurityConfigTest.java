package se.jobtechdev.mininskrivningsstatus.api.config;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.AuthorizeHttpRequestsConfigurer;
import org.springframework.security.config.annotation.web.configurers.SessionManagementConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import se.jobtechdev.mininskrivningsstatus.api.security.RequestHeaderAuthenticationProvider;

@ExtendWith(MockitoExtension.class)
public class SecurityConfigTest {
  private SecurityConfig securityConfig;

  @Mock private RequestHeaderAuthenticationProvider requestHeaderAuthenticationProvider;

  @BeforeEach
  void setUp() {
    securityConfig = new SecurityConfig(requestHeaderAuthenticationProvider);
  }

  @Test
  public void getAuthorizeHttpRequestsCustomizer() {
    // Arrange
    final var mockRegistry =
        mock(AuthorizeHttpRequestsConfigurer.AuthorizationManagerRequestMatcherRegistry.class);
    final var mockGetError = mock(AuthorizeHttpRequestsConfigurer.AuthorizedUrl.class);
    final var mockGetHealth = mock(AuthorizeHttpRequestsConfigurer.AuthorizedUrl.class);
    final var mockGetApiInfo = mock(AuthorizeHttpRequestsConfigurer.AuthorizedUrl.class);
    final var mockGetPersonRegistrationStatus =
        mock(AuthorizeHttpRequestsConfigurer.AuthorizedUrl.class);

    when(mockRegistry.requestMatchers(HttpMethod.GET, "/error")).thenReturn(mockGetError);
    when(mockRegistry.requestMatchers(HttpMethod.GET, "/health")).thenReturn(mockGetHealth);
    when(mockRegistry.requestMatchers(HttpMethod.GET, "/api-info")).thenReturn(mockGetApiInfo);
    when(mockRegistry.requestMatchers(HttpMethod.GET, "/persons/*/registrationStatus"))
        .thenReturn(mockGetPersonRegistrationStatus);

    when(mockGetError.permitAll()).thenReturn(mockRegistry);
    when(mockGetHealth.permitAll()).thenReturn(mockRegistry);
    when(mockGetApiInfo.permitAll()).thenReturn(mockRegistry);
    when(mockGetPersonRegistrationStatus.hasAnyAuthority("client")).thenReturn(mockRegistry);

    // Act
    final var authorizeHttpRequestsCustomizer = securityConfig.getAuthorizeHttpRequestsCustomizer();
    authorizeHttpRequestsCustomizer.customize(mockRegistry);

    // Assert
    verify(mockRegistry, times(4)).requestMatchers(Mockito.any(), Mockito.any());
    verify(mockRegistry, times(1)).requestMatchers(HttpMethod.GET, "/error");
    verify(mockRegistry, times(1)).requestMatchers(HttpMethod.GET, "/health");
    verify(mockRegistry, times(1)).requestMatchers(HttpMethod.GET, "/api-info");
    verify(mockRegistry, times(1)).requestMatchers(HttpMethod.GET, "/persons/*/registrationStatus");

    verify(mockGetError, times(1)).permitAll();
    verify(mockGetError, times(0)).hasAnyAuthority(any());

    verify(mockGetHealth, times(1)).permitAll();
    verify(mockGetHealth, times(0)).hasAnyAuthority(any());

    verify(mockGetApiInfo, times(1)).permitAll();
    verify(mockGetError, times(0)).hasAnyAuthority(any());

    verify(mockGetPersonRegistrationStatus, times(0)).permitAll();
    verify(mockGetPersonRegistrationStatus, times(1)).hasAnyAuthority("client");
  }

  @Test
  public void getSessionManagementConfigurerCustomizer() {
    // Assemble
    final var mockSessionManagementConfigurer = mock(SessionManagementConfigurer.class);

    // Act
    final var sessionManagementConfigurerCustomizer =
        securityConfig.getSessionManagementConfigurerCustomizer();
    sessionManagementConfigurerCustomizer.customize(mockSessionManagementConfigurer);

    // Assert
    verify(mockSessionManagementConfigurer, times(1))
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
  }

  @Test
  public void filterChain() throws Exception {
    // Assert
    final var mockHttpSecurity = mock(HttpSecurity.class);
    final var constructionProviderManagerArgs = new HashMap<ProviderManager, List<?>>();
    try (final var staticCustomizer = mockStatic(Customizer.class);
        final var staticAbstractHttpConfigurer = mockStatic(AbstractHttpConfigurer.class);
        final var constructionRequestHeaderAuthenticationFilter =
            mockConstruction(RequestHeaderAuthenticationFilter.class);
        final var constructionAntPathRequestMatcher =
            mockConstruction(AntPathRequestMatcher.class);
        final var constructionProviderManager =
            mockConstruction(
                ProviderManager.class,
                (mock, context) -> {
                  constructionProviderManagerArgs.put(mock, context.arguments());
                })) {
      final var mockCorsDefaults = mock(Customizer.class);
      staticCustomizer.when(Customizer::withDefaults).thenReturn(mockCorsDefaults);

      final var mockSecurityFilterChain = mock(DefaultSecurityFilterChain.class);
      when(mockHttpSecurity.cors(any())).thenReturn(mockHttpSecurity);
      when(mockHttpSecurity.csrf(any())).thenReturn(mockHttpSecurity);
      when(mockHttpSecurity.sessionManagement(any())).thenReturn(mockHttpSecurity);
      when(mockHttpSecurity.addFilterBefore(any(), any())).thenReturn(mockHttpSecurity);
      when(mockHttpSecurity.authorizeHttpRequests(any())).thenReturn(mockHttpSecurity);
      when(mockHttpSecurity.build()).thenReturn(mockSecurityFilterChain);

      // Act
      final var result = securityConfig.filterChain(mockHttpSecurity);

      // Assert
      assertEquals(mockSecurityFilterChain, result);
      assertEquals(1, constructionProviderManager.constructed().size());
      final var mockProviderManager = constructionProviderManager.constructed().get(0);
      assertEquals(
          Collections.singletonList(requestHeaderAuthenticationProvider),
          constructionProviderManagerArgs.get(mockProviderManager).get(0));

      assertEquals(1, constructionAntPathRequestMatcher.constructed().size());
      final var mockAntPathRequestMatcher = constructionAntPathRequestMatcher.constructed().get(0);

      assertEquals(1, constructionRequestHeaderAuthenticationFilter.constructed().size());
      final var mockRequestHeaderAuthenticationFilter =
          constructionRequestHeaderAuthenticationFilter.constructed().get(0);
      verify(mockRequestHeaderAuthenticationFilter, times(1))
          .setPrincipalRequestHeader("X-Auth-Key");
      verify(mockRequestHeaderAuthenticationFilter, times(1))
          .setRequiresAuthenticationRequestMatcher(mockAntPathRequestMatcher);
      verify(mockRequestHeaderAuthenticationFilter, times(1))
          .setAuthenticationManager(mockProviderManager);
    }
  }
}
