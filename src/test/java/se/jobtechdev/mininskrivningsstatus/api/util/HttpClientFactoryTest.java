package se.jobtechdev.mininskrivningsstatus.api.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import java.util.HashMap;
import java.util.List;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.HttpHost;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

public class HttpClientFactoryTest {
  @Test
  @Tag("Unit")
  public void createHttpClient_withoutProxyConfiguration() {
    // Arrange
    final var mockHttpClientBuilder = mock(HttpClientBuilder.class);
    final var mockCloseableHttpClient = mock(CloseableHttpClient.class);

    when(mockHttpClientBuilder.build()).thenReturn(mockCloseableHttpClient);

    final var constructionHttpHostArgs = new HashMap<HttpHost, List<?>>();

    try (final var staticHttpClients = mockStatic(HttpClients.class);
        final var constructionHttpHost =
            mockConstruction(
                HttpHost.class,
                (mock, ctx) -> {
                  constructionHttpHostArgs.put(mock, ctx.arguments());
                })) {
      staticHttpClients.when(() -> HttpClients.custom()).thenReturn(mockHttpClientBuilder);

      // Act
      final var httpClient = HttpClientFactory.createHttpClient(null);

      // Assert
      assertEquals(0, constructionHttpHost.constructed().size());

      verify(mockHttpClientBuilder, times(0)).setProxy(any());
      verify(mockHttpClientBuilder, times(1)).build();

      assertEquals(mockCloseableHttpClient, httpClient);
    }
  }

  @Test
  @Tag("Unit")
  public void createHttpClient_withProxyConfiguration() {
    // Arrange
    final var mockHttpClientBuilder = mock(HttpClientBuilder.class);
    final var mockCloseableHttpClient = mock(CloseableHttpClient.class);

    when(mockHttpClientBuilder.build()).thenReturn(mockCloseableHttpClient);

    final var constructionHttpHostArgs = new HashMap<HttpHost, List<?>>();

    try (final var staticHttpClients = mockStatic(HttpClients.class);
        final var constructionHttpHost =
            mockConstruction(
                HttpHost.class,
                (mock, ctx) -> {
                  constructionHttpHostArgs.put(mock, ctx.arguments());
                })) {
      staticHttpClients.when(() -> HttpClients.custom()).thenReturn(mockHttpClientBuilder);

      // Act
      final var httpClient =
          HttpClientFactory.createHttpClient(
              new HttpClientFactory.ProxyConfiguration("http", "example.com", 1234));

      // Assert
      assertEquals(1, constructionHttpHost.constructed().size());

      final var mockHttpHost = constructionHttpHost.constructed().getFirst();

      assertEquals("http", constructionHttpHostArgs.get(mockHttpHost).get(0));
      assertEquals("example.com", constructionHttpHostArgs.get(mockHttpHost).get(1));
      assertEquals(1234, constructionHttpHostArgs.get(mockHttpHost).get(2));

      verify(mockHttpClientBuilder, times(1)).setProxy(mockHttpHost);
      verify(mockHttpClientBuilder, times(1)).build();

      assertEquals(mockCloseableHttpClient, httpClient);
    }
  }
}
