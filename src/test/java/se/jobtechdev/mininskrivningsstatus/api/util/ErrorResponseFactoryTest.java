package se.jobtechdev.mininskrivningsstatus.api.util;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import se.jobtechdev.mininskrivningsstatus.api.generated.model.ErrorResponse;

import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class ErrorResponseFactoryTest {
    @Test
    public void construct() {
        new ErrorResponseFactory();
    }

    @Test
    public void createErrorResponse() {
        // Arrange
        final var mockHttpStatus = mock(HttpStatus.class);
        when(mockHttpStatus.value()).thenReturn(123);
        when(mockHttpStatus.getReasonPhrase()).thenReturn("Example reason phrase");

        final var constructionErrorResponseArgs = new HashMap<ErrorResponse, List<?>>();
        try (final var constructionErrorResponse = mockConstruction(ErrorResponse.class, (mock, context) -> {
            constructionErrorResponseArgs.put(mock, context.arguments());
        })) {
            // Act
            final var result = ErrorResponseFactory.createErrorResponse(mockHttpStatus);

            // Assert
            assertEquals(1, constructionErrorResponse.constructed().size());
            final var mockErrorResponse = constructionErrorResponse.constructed().get(0);
            assertEquals(123, constructionErrorResponseArgs.get(mockErrorResponse).get(0));
            assertEquals("Example reason phrase", constructionErrorResponseArgs.get(mockErrorResponse).get(1));
            assertEquals(mockErrorResponse, result);
        }
    }

    @Test
    public void createErrorResponse_withMessage() {
        // Arrange
        final var mockHttpStatus = mock(HttpStatus.class);
        when(mockHttpStatus.value()).thenReturn(123);
        when(mockHttpStatus.getReasonPhrase()).thenReturn("Example reason phrase");

        final var constructionErrorResponseArgs = new HashMap<ErrorResponse, List<?>>();
        try (final var constructionErrorResponse = mockConstruction(ErrorResponse.class, (mock, context) -> {
            constructionErrorResponseArgs.put(mock, context.arguments());
            when(mock.message("An example message")).thenReturn(mock);
        })) {
            // Act
            final var result = ErrorResponseFactory.createErrorResponse(mockHttpStatus, "An example message");

            // Assert
            assertEquals(1, constructionErrorResponse.constructed().size());
            final var mockErrorResponse = constructionErrorResponse.constructed().get(0);
            assertEquals(123, constructionErrorResponseArgs.get(mockErrorResponse).get(0));
            assertEquals("Example reason phrase", constructionErrorResponseArgs.get(mockErrorResponse).get(1));
            verify(mockErrorResponse, times(1)).message("An example message");
            assertEquals(mockErrorResponse, result);
        }
    }
}
