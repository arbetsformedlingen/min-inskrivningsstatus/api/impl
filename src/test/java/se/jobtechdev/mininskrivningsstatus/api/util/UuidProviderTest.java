package se.jobtechdev.mininskrivningsstatus.api.util;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;

public class UuidProviderTest {
    @Test
    public void uuid() {
        final var mockUuid = mock(UUID.class);

        try (final var staticUUID = mockStatic(UUID.class)) {
            staticUUID.when(() -> UUID.randomUUID()).thenReturn(mockUuid);

            // Act
            final var result = UuidProvider.uuid();

            // Assert
            assertEquals(mockUuid, result);
        }
    }
}
