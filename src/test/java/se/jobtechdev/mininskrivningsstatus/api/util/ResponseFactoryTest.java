package se.jobtechdev.mininskrivningsstatus.api.util;

import org.junit.jupiter.api.Test;
import se.jobtechdev.mininskrivningsstatus.api.generated.model.*;

import java.net.URL;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class ResponseFactoryTest {
  @Test
  public void getApiInfoResponse() {
    // Arrange
    final var mockApiName = "mockApiName";
    final var mockApiVersion = "mockApiVersion";
    final var mockBuildVersion = "mockBuildVersion";
    final var mockApiReleased = mock(LocalDate.class);
    final var mockApiDocumentation = mock(URL.class);
    final var mockApiStatus = mock(ApiStatus.class);

    final var constructionGetApiInfoResponseArgs = new HashMap<GetApiInfoResponse, List<?>>();

    try (final var constructionGetApiInfoResponse =
             mockConstruction(
                 GetApiInfoResponse.class,
                 (mock, ctx) -> constructionGetApiInfoResponseArgs.put(mock, ctx.arguments()))) {

      // Act
      final var response =
          ResponseFactory.getApiInfoResponse(
              mockApiName,
              mockApiVersion,
              mockBuildVersion,
              mockApiReleased,
              mockApiDocumentation,
              mockApiStatus);

      assertEquals(1, constructionGetApiInfoResponse.constructed().size());

      final var mockGetApiInfoResponse = constructionGetApiInfoResponse.constructed().getFirst();

      assertEquals(6, constructionGetApiInfoResponseArgs.get(mockGetApiInfoResponse).size());

      assertEquals(
          mockApiName, constructionGetApiInfoResponseArgs.get(mockGetApiInfoResponse).get(0));

      assertEquals(
          mockApiVersion, constructionGetApiInfoResponseArgs.get(mockGetApiInfoResponse).get(1));

      assertEquals(
          mockBuildVersion, constructionGetApiInfoResponseArgs.get(mockGetApiInfoResponse).get(2));

      assertEquals(
          mockApiReleased, constructionGetApiInfoResponseArgs.get(mockGetApiInfoResponse).get(3));

      assertEquals(
          mockApiDocumentation,
          constructionGetApiInfoResponseArgs.get(mockGetApiInfoResponse).get(4));

      assertEquals(
          mockApiStatus, constructionGetApiInfoResponseArgs.get(mockGetApiInfoResponse).get(5));
    }
  }

  @Test
  public void getHealthResponse() {
    // Arrange
    final var constructionGetHealthResponseArgs = new HashMap<GetHealthResponse, List<?>>();
    final var mockStatus = mock(Status.class);
    final var mockHealthChecks = mock(HealthChecks.class);

    try (final var constructionGetHealthResponse =
             mockConstruction(
                 GetHealthResponse.class,
                 (mock, ctx) -> constructionGetHealthResponseArgs.put(mock, ctx.arguments()))) {

      // Act
      final var response = ResponseFactory.getHealthResponse(mockStatus, mockHealthChecks);

      assertEquals(1, constructionGetHealthResponse.constructed().size());

      final var mockGetHealthResponse = constructionGetHealthResponse.constructed().getFirst();

      assertEquals(2, constructionGetHealthResponseArgs.get(mockGetHealthResponse).size());

      assertEquals(mockStatus, constructionGetHealthResponseArgs.get(mockGetHealthResponse).get(0));

      assertEquals(
          mockHealthChecks, constructionGetHealthResponseArgs.get(mockGetHealthResponse).get(1));
    }
  }
}
