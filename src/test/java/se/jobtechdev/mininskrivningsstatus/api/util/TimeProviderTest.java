package se.jobtechdev.mininskrivningsstatus.api.util;

import org.junit.jupiter.api.Test;

import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;

public class TimeProviderTest {
    @Test
    public void currentTimeMillis() {
        assertNotNull(TimeProvider.currentTimeMillis());
    }

    @Test
    public void now() {
        // Arrange
        final var mockZonedDateTime = mock(ZonedDateTime.class);

        try (final var staticZonedDateTime = mockStatic(ZonedDateTime.class)) {
            staticZonedDateTime.when(() -> ZonedDateTime.now()).thenReturn(mockZonedDateTime);

            // Act
            final var result = TimeProvider.now();

            // Assert
            assertEquals(mockZonedDateTime, result);
        }
    }
}
