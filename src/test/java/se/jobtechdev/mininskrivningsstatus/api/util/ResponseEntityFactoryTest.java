package se.jobtechdev.mininskrivningsstatus.api.util;

import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;
import se.jobtechdev.mininskrivningsstatus.api.controller.ControllerExceptionHandler;
import se.jobtechdev.mininskrivningsstatus.api.generated.model.ErrorResponse;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class ResponseEntityFactoryTest {
    @Test
    public void construct() {
        new ResponseEntityFactory();
    }

    @Test
    public void create() {
        // Arrange
        final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
        final var mockResponseEntity = mock(ResponseEntity.class);
        final var mockErrorResponse = mock(ErrorResponse.class);
        when(mockErrorResponse.getStatus()).thenReturn(123);

        try (final var staticResponseEntity = mockStatic(ResponseEntity.class)) {
            staticResponseEntity.when(() -> ResponseEntity.status(123)).thenReturn(mockResponseEntityBodyBuilder);
            when(mockResponseEntityBodyBuilder.body(mockErrorResponse)).thenReturn(mockResponseEntity);

            // Act
            final var result = ResponseEntityFactory.create(mockErrorResponse);

            // Assert
            staticResponseEntity.verify(() -> ResponseEntity.status(123), times(1));
            verify(mockResponseEntityBodyBuilder, times(1)).body(mockErrorResponse);
            assertEquals(mockResponseEntity, result);
        }
    }
}
