package se.jobtechdev.mininskrivningsstatus.api.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import java.util.HashMap;
import java.util.List;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import se.arbetsformedlingen.rask.api.DefaultApi;
import se.arbetsformedlingen.rask.client.ApiClient;

public class RaskApiFactoryTest {
  @Test
  @Tag("Unit")
  public void construct() {
    new RaskApiFactory("example-base-url", null, null, null);
  }

  @Test
  @Tag("Unit")
  public void construct_withProxyProtocolHostAndPort() {
    // Arrange
    final var constructionProxyConfigurationArgs =
        new HashMap<HttpClientFactory.ProxyConfiguration, List<?>>();

    try (final var constructionProxyConfiguration =
        mockConstruction(
            HttpClientFactory.ProxyConfiguration.class,
            (mock, ctx) -> {
              constructionProxyConfigurationArgs.put(mock, ctx.arguments());
            })) {
      // Act
      new RaskApiFactory("example-base-url", "http", "example.com", 1234);

      // Assert
      assertEquals(1, constructionProxyConfiguration.constructed().size());

      final var mockProxyConfiguration = constructionProxyConfiguration.constructed().getFirst();

      assertEquals("http", constructionProxyConfigurationArgs.get(mockProxyConfiguration).get(0));
      assertEquals(
          "example.com", constructionProxyConfigurationArgs.get(mockProxyConfiguration).get(1));
      assertEquals(1234, constructionProxyConfigurationArgs.get(mockProxyConfiguration).get(2));
    }
  }

  @Test
  @Tag("Unit")
  public void construct_withProxyProtocolAndHost() {
    // Arrange
    try (final var constructionProxyConfiguration =
        mockConstruction(HttpClientFactory.ProxyConfiguration.class)) {
      // Act
      new RaskApiFactory("example-base-url", "http", "example.com", null);

      // Assert
      assertEquals(0, constructionProxyConfiguration.constructed().size());
    }
  }

  @Test
  @Tag("Unit")
  public void construct_withProxyProtocol() {
    // Arrange
    try (final var constructionProxyConfiguration =
        mockConstruction(HttpClientFactory.ProxyConfiguration.class)) {
      // Act
      new RaskApiFactory("example-base-url", "http", null, null);

      // Assert
      assertEquals(0, constructionProxyConfiguration.constructed().size());
    }
  }

  @Test
  @Tag("Unit")
  public void createClient_withProxyProtocolHostAndPort() {
    // Arrange
    final var mockCloseableHttpClient = mock(CloseableHttpClient.class);

    final var constructionProxyConfigurationArgs =
        new HashMap<HttpClientFactory.ProxyConfiguration, List<?>>();
    final var constructionApiClientArgs = new HashMap<ApiClient, List<?>>();
    final var constructionDefaultApiArgs = new HashMap<DefaultApi, List<?>>();

    try (final var constructionProxyConfiguration =
            mockConstruction(
                HttpClientFactory.ProxyConfiguration.class,
                (mock, ctx) -> {
                  constructionProxyConfigurationArgs.put(mock, ctx.arguments());
                });
        final var staticHttpClientFactory = mockStatic(HttpClientFactory.class);
        final var constructionApiClient =
            mockConstruction(
                ApiClient.class,
                (mock, ctx) -> {
                  constructionApiClientArgs.put(mock, ctx.arguments());
                });
        final var constructionDefaultApi =
            mockConstruction(
                DefaultApi.class,
                (mock, ctx) -> {
                  constructionDefaultApiArgs.put(mock, ctx.arguments());
                })) {
      staticHttpClientFactory
          .when(
              () ->
                  HttpClientFactory.createHttpClient(
                      any(HttpClientFactory.ProxyConfiguration.class)))
          .thenReturn(mockCloseableHttpClient);
      // Act
      final var baseUrl = "example-base-url";
      final var proxyProtocol = "http";
      final var proxyHost = "example.com";
      final var proxyPort = 1234;
      final var raskApiFactory = new RaskApiFactory(baseUrl, proxyProtocol, proxyHost, proxyPort);

      final var raskApi = raskApiFactory.createClient();
      final var raskApi2 = raskApiFactory.createClient();

      // Assert
      assertEquals(1, constructionProxyConfiguration.constructed().size());

      final var mockProxyConfiguration = constructionProxyConfiguration.constructed().getFirst();

      assertEquals(
          proxyProtocol, constructionProxyConfigurationArgs.get(mockProxyConfiguration).get(0));
      assertEquals(
          proxyHost, constructionProxyConfigurationArgs.get(mockProxyConfiguration).get(1));
      assertEquals(
          proxyPort, constructionProxyConfigurationArgs.get(mockProxyConfiguration).get(2));

      staticHttpClientFactory.verify(
          () -> HttpClientFactory.createHttpClient(mockProxyConfiguration), times(1));

      assertEquals(1, constructionApiClient.constructed().size());

      final var mockApiClient = constructionApiClient.constructed().getFirst();

      assertEquals(
          mockCloseableHttpClient, constructionApiClientArgs.get(mockApiClient).getFirst());

      verify(mockApiClient, times(1)).setBasePath(baseUrl);

      assertEquals(1, constructionDefaultApi.constructed().size());

      final var mockDefaultApi = constructionDefaultApi.constructed().getFirst();

      assertEquals(mockApiClient, constructionDefaultApiArgs.get(mockDefaultApi).getFirst());

      assertEquals(mockDefaultApi, raskApi);

      assertEquals(raskApi2, raskApi);
    }
  }
}
