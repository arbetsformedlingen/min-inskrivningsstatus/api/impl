package se.jobtechdev.mininskrivningsstatus.api.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import se.arbetsformedlingen.rask.model.ArbetsokandeAkassaDTOKompositArbetssokande;
import se.arbetsformedlingen.rask.model.ArbetsokandeProcessStatusDTOKomposit;
import se.arbetsformedlingen.rask.model.ArbetsokandeProcessStatusDTOKompositProcessStatus;
import se.jobtechdev.mininskrivningsstatus.api.exception.ApiException;
import se.jobtechdev.mininskrivningsstatus.api.generated.model.ErrorResponse;
import se.jobtechdev.mininskrivningsstatus.api.generated.model.GetPersonRegistrationStatusResponse;
import se.jobtechdev.mininskrivningsstatus.api.util.ErrorResponseFactory;
import se.jobtechdev.mininskrivningsstatus.api.util.LoggerSupplier;

public class DataServiceTest {
  static Stream<Arguments> argsProviderFactory() {
    final var personId = "personId";
    return Stream.of(
        Arguments.of(
            new DataService.PersonRegistrationStatus(
                personId, true, LocalDate.parse("2000-01-01"), LocalDate.parse("2000-02-01")),
            1),
        Arguments.of(
            new DataService.PersonRegistrationStatus(
                personId, true, LocalDate.parse("2000-01-01"), null),
            0),
        Arguments.of(
            new DataService.PersonRegistrationStatus(
                personId, true, null, LocalDate.parse("2000-02-01")),
            1),
        Arguments.of(new DataService.PersonRegistrationStatus(personId, true, null, null), 1),
        Arguments.of(
            new DataService.PersonRegistrationStatus(
                personId, false, LocalDate.parse("2000-01-01"), LocalDate.parse("2000-02-01")),
            0),
        Arguments.of(
            new DataService.PersonRegistrationStatus(
                personId, false, LocalDate.parse("2000-01-01"), null),
            1),
        Arguments.of(
            new DataService.PersonRegistrationStatus(
                personId, false, null, LocalDate.parse("2000-02-01")),
            0),
        Arguments.of(new DataService.PersonRegistrationStatus(personId, false, null, null), 0));
  }

  @Test
  public void
      getPersonRegistrationStatus_whenRegistrationStatusIsEmpty_return_registrationStatus_false() {
    // Arrange
    final var mockRaskService = mock(RaskService.class);
    when(mockRaskService.getRegistrationStatus("example-person-id")).thenReturn(Optional.empty());

    final var dataService = new DataService(mockRaskService);

    // Act
    final var result = dataService.getPersonRegistrationStatus("example-person-id");

    // Assert
    assertEquals(
        new DataService.PersonRegistrationStatus("example-person-id", false, null, null), result);
  }

  @Test
  public void
      getPersonRegistrationStatus_whenDataInconsistencyIsDetectedPersonIdDoesNotMatch_throwApiExceptionInternalServerError() {
    // Arrange
    final var mockRaskService = mock(RaskService.class);
    final var mockArbetsokandeAkassaDTOKompositArbetssokande =
        mock(ArbetsokandeAkassaDTOKompositArbetssokande.class);
    final var mockRegistrationStatus = mock(ArbetsokandeProcessStatusDTOKomposit.class);
    when(mockRaskService.getRegistrationStatus("example-person-id"))
        .thenReturn(Optional.of(mockRegistrationStatus));
    when(mockRegistrationStatus.getArbetssokande())
        .thenReturn(mockArbetsokandeAkassaDTOKompositArbetssokande);
    when(mockArbetsokandeAkassaDTOKompositArbetssokande.getPersonnummer())
        .thenReturn("other-person-id");

    final var dataService = new DataService(mockRaskService);
    final var constructionApiExceptionArgs = new HashMap<ApiException, List<?>>();
    try (final var staticErrorResponseFactory = mockStatic(ErrorResponseFactory.class);
        final var constructionApiException =
            mockConstruction(
                ApiException.class,
                (mock, context) -> {
                  constructionApiExceptionArgs.put(mock, context.arguments());
                })) {
      final var mockErrorResponse = mock(ErrorResponse.class);
      staticErrorResponseFactory
          .when(() -> ErrorResponseFactory.createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR))
          .thenReturn(mockErrorResponse);

      // Act
      final var thrown =
          assertThrows(
              ApiException.class,
              () -> dataService.getPersonRegistrationStatus("example-person-id"));

      // Assert
      assertEquals(1, constructionApiException.constructed().size());
      final var mockApiException = constructionApiException.constructed().get(0);
      assertEquals(mockErrorResponse, constructionApiExceptionArgs.get(mockApiException).get(0));
      assertEquals(thrown, mockApiException);
    }
  }

  @Test
  public void getPersonRegistrationStatus_whenRegistered_returnPositiveResult() {
    // Arrange
    final var mockRaskService = mock(RaskService.class);
    final var mockArbetsokandeAkassaDTOKompositArbetssokande =
        mock(ArbetsokandeAkassaDTOKompositArbetssokande.class);
    final var mockRegistrationStatus = mock(ArbetsokandeProcessStatusDTOKomposit.class);
    when(mockRaskService.getRegistrationStatus("example-person-id"))
        .thenReturn(Optional.of(mockRegistrationStatus));

    when(mockRegistrationStatus.getArbetssokande())
        .thenReturn(mockArbetsokandeAkassaDTOKompositArbetssokande);
    when(mockArbetsokandeAkassaDTOKompositArbetssokande.getPersonnummer())
        .thenReturn("example-person-id");
    final var mockArbetsokandeProcessStatusDTOKompositProcessStatus =
        mock(ArbetsokandeProcessStatusDTOKompositProcessStatus.class);
    when(mockArbetsokandeProcessStatusDTOKompositProcessStatus.getSparStatus())
        .thenReturn(
            ArbetsokandeProcessStatusDTOKompositProcessStatus.SparStatusEnum.valueOf("INSKRIVEN"));
    when(mockArbetsokandeProcessStatusDTOKompositProcessStatus.getAktuellSedanDatum())
        .thenReturn(LocalDate.parse("2024-01-01"));
    when(mockRegistrationStatus.getProcessStatus())
        .thenReturn(mockArbetsokandeProcessStatusDTOKompositProcessStatus);

    // Act
    final var dataService = new DataService(mockRaskService);
    final var result = dataService.getPersonRegistrationStatus("example-person-id");

    // Assert
    assertEquals(
        new DataService.PersonRegistrationStatus(
            "example-person-id", true, LocalDate.parse("2024-01-01"), null),
        result);
  }

  @Test
  public void getPersonRegistrationStatus_whenNotRegistered_returnNegativeResult() {
    // Arrange
    final var mockRaskService = mock(RaskService.class);
    final var mockArbetsokandeAkassaDTOKompositArbetssokande =
        mock(ArbetsokandeAkassaDTOKompositArbetssokande.class);
    final var mockRegistrationStatus = mock(ArbetsokandeProcessStatusDTOKomposit.class);
    when(mockRaskService.getRegistrationStatus("example-person-id"))
        .thenReturn(Optional.of(mockRegistrationStatus));

    when(mockRegistrationStatus.getArbetssokande())
        .thenReturn(mockArbetsokandeAkassaDTOKompositArbetssokande);
    when(mockArbetsokandeAkassaDTOKompositArbetssokande.getPersonnummer())
        .thenReturn("example-person-id");
    final var mockArbetsokandeProcessStatusDTOKompositProcessStatus =
        mock(ArbetsokandeProcessStatusDTOKompositProcessStatus.class);
    when(mockArbetsokandeProcessStatusDTOKompositProcessStatus.getSparStatus())
        .thenReturn(
            ArbetsokandeProcessStatusDTOKompositProcessStatus.SparStatusEnum.valueOf(
                "OREGISTRERAD"));
    when(mockArbetsokandeProcessStatusDTOKompositProcessStatus.getAktuellSedanDatum())
        .thenReturn(LocalDate.parse("2024-01-01"));
    when(mockArbetsokandeProcessStatusDTOKompositProcessStatus.getAvaktualiseringsDatum())
        .thenReturn(LocalDate.parse("2024-05-01"));
    when(mockRegistrationStatus.getProcessStatus())
        .thenReturn(mockArbetsokandeProcessStatusDTOKompositProcessStatus);

    // Act
    final var dataService = new DataService(mockRaskService);
    final var result = dataService.getPersonRegistrationStatus("example-person-id");

    // Assert
    assertEquals(
        new DataService.PersonRegistrationStatus(
            "example-person-id",
            false,
            LocalDate.parse("2024-01-01"),
            LocalDate.parse("2024-05-01")),
        result);
  }

  @Test
  public void
      getPersonRegistrationStatus_whenNotRegisteredAndNoRegistrationDatePresent_returnNegativeResult() {
    // Arrange
    final var mockRaskService = mock(RaskService.class);
    final var mockArbetsokandeAkassaDTOKompositArbetssokande =
        mock(ArbetsokandeAkassaDTOKompositArbetssokande.class);
    final var mockRegistrationStatus = mock(ArbetsokandeProcessStatusDTOKomposit.class);
    when(mockRaskService.getRegistrationStatus("example-person-id"))
        .thenReturn(Optional.of(mockRegistrationStatus));

    when(mockRegistrationStatus.getArbetssokande())
        .thenReturn(mockArbetsokandeAkassaDTOKompositArbetssokande);
    when(mockArbetsokandeAkassaDTOKompositArbetssokande.getPersonnummer())
        .thenReturn("example-person-id");
    final var mockArbetsokandeProcessStatusDTOKompositProcessStatus =
        mock(ArbetsokandeProcessStatusDTOKompositProcessStatus.class);
    when(mockArbetsokandeProcessStatusDTOKompositProcessStatus.getSparStatus())
        .thenReturn(
            ArbetsokandeProcessStatusDTOKompositProcessStatus.SparStatusEnum.valueOf(
                "OREGISTRERAD"));
    when(mockArbetsokandeProcessStatusDTOKompositProcessStatus.getAktuellSedanDatum())
        .thenReturn(null);
    when(mockArbetsokandeProcessStatusDTOKompositProcessStatus.getAvaktualiseringsDatum())
        .thenReturn(null);
    when(mockRegistrationStatus.getProcessStatus())
        .thenReturn(mockArbetsokandeProcessStatusDTOKompositProcessStatus);

    // Act
    final var dataService = new DataService(mockRaskService);
    final var result = dataService.getPersonRegistrationStatus("example-person-id");

    // Assert
    assertEquals(
        new DataService.PersonRegistrationStatus("example-person-id", false, null, null), result);
  }

  @Test
  public void createJsonResponse_whenNeverRegistered() {
    final var args = new HashMap<GetPersonRegistrationStatusResponse, List<?>>();
    try (final var construction =
        mockConstruction(
            GetPersonRegistrationStatusResponse.class,
            (mock, context) -> {
              args.put(mock, context.arguments());
            })) {
      // Arrange
      final var mockRaskService = mock(RaskService.class);
      final var status =
          new DataService.PersonRegistrationStatus("example-person-id", false, null, null);

      // Act
      final var dataService = new DataService(mockRaskService);
      final var result = dataService.createJsonResponse(status);

      // Assert
      assertEquals(1, construction.constructed().size());
      final var statusResponse = construction.constructed().get(0);

      assertEquals(statusResponse, result);
      verify(statusResponse, times(0)).setRegistrationDate(any());
      verify(statusResponse, times(0)).setDeregistrationDate(any());
    }
  }

  @Test
  public void createJsonResponse_whenNotRegistered() {
    final var args = new HashMap<GetPersonRegistrationStatusResponse, List<?>>();
    try (final var construction =
        mockConstruction(
            GetPersonRegistrationStatusResponse.class,
            (mock, context) -> {
              args.put(mock, context.arguments());
            })) {
      // Arrange
      final var mockRaskService = mock(RaskService.class);
      final var mockRegistrationDate = mock(LocalDate.class);
      when(mockRegistrationDate.toString()).thenReturn("2024-01-12");
      final var mockDeregistrationDate = mock(LocalDate.class);
      when(mockDeregistrationDate.toString()).thenReturn("2024-05-12");
      final var status =
          new DataService.PersonRegistrationStatus(
              "example-person-id", false, mockRegistrationDate, mockDeregistrationDate);

      // Act
      final var dataService = new DataService(mockRaskService);
      final var result = dataService.createJsonResponse(status);

      // Assert
      assertEquals(1, construction.constructed().size());
      final var statusResponse = construction.constructed().get(0);

      assertEquals(statusResponse, result);
      verify(statusResponse, times(1)).setRegistrationDate(any());
      verify(statusResponse, times(1)).setDeregistrationDate(any());
    }
  }

  @Test
  public void createJsonResponse_whenRegistered() {
    final var args = new HashMap<GetPersonRegistrationStatusResponse, List<?>>();
    try (final var staticLocalDate = mockStatic(LocalDate.class);
        final var construction =
            mockConstruction(
                GetPersonRegistrationStatusResponse.class,
                (mock, context) -> {
                  args.put(mock, context.arguments());
                })) {
      // Arrange
      final var mockRaskService = mock(RaskService.class);
      final var mockRegistrationDate = mock(LocalDate.class);
      when(mockRegistrationDate.toString()).thenReturn("2024-01-12");
      final var status =
          new DataService.PersonRegistrationStatus(
              "example-person-id", true, mockRegistrationDate, null);

      // Act
      final var dataService = new DataService(mockRaskService);
      final var result = dataService.createJsonResponse(status);

      // Assert
      assertEquals(1, construction.constructed().size());
      final var statusResponse = construction.constructed().get(0);

      assertEquals(statusResponse, result);
      verify(statusResponse, times(1)).setRegistrationDate(any());
      verify(statusResponse, times(0)).setDeregistrationDate(any());
    }
  }

  @Test
  public void createTextResponse_whenNeverRegistered() {
    final var args = new HashMap<GetPersonRegistrationStatusResponse, List<?>>();
    try (final var construction =
        mockConstruction(
            GetPersonRegistrationStatusResponse.class,
            (mock, context) -> {
              args.put(mock, context.arguments());
            })) {
      // Arrange
      final var mockRaskService = mock(RaskService.class);
      final var status =
          new DataService.PersonRegistrationStatus("example-person-id", false, null, null);

      // Act
      final var dataService = new DataService(mockRaskService);
      final var result = dataService.createTextResponse(status);
      final var statusResponse = "Ej inskriven";
      // Assert
      assertEquals(statusResponse, result);
    }
  }

  @Test
  public void createTextResponse_whenNotRegistered() {
    final var args = new HashMap<GetPersonRegistrationStatusResponse, List<?>>();
    try (final var construction =
        mockConstruction(
            GetPersonRegistrationStatusResponse.class,
            (mock, context) -> {
              args.put(mock, context.arguments());
            })) {
      // Arrange
      final var mockRaskService = mock(RaskService.class);
      final var mockRegistrationDate = mock(LocalDate.class);
      when(mockRegistrationDate.toString()).thenReturn("2024-01-12");
      final var mockDeregistrationDate = mock(LocalDate.class);
      when(mockDeregistrationDate.toString()).thenReturn("2024-05-12");
      final var status =
          new DataService.PersonRegistrationStatus(
              "example-person-id", false, mockRegistrationDate, mockDeregistrationDate);

      // Act
      final var dataService = new DataService(mockRaskService);
      final var result = dataService.createTextResponse(status);
      final var statusResponse = "Ej inskriven, registrerad 2024-01-12, avregistrerad 2024-05-12";
      // Assert
      assertEquals(statusResponse, result);
    }
  }

  @Test
  public void createTextResponse_whenRegistered() {
    final var args = new HashMap<GetPersonRegistrationStatusResponse, List<?>>();
    try (final var staticLocalDate = mockStatic(LocalDate.class)) {
      // Arrange
      final var mockRaskService = mock(RaskService.class);
      final var mockRegistrationDate = mock(LocalDate.class);
      when(mockRegistrationDate.toString()).thenReturn("2024-01-12");
      final var status =
          new DataService.PersonRegistrationStatus(
              "example-person-id", true, mockRegistrationDate, null);

      // Act
      final var dataService = new DataService(mockRaskService);
      final var result = dataService.createTextResponse(status);

      // Assert
      final var statusResponse = "Inskriven, registrerad 2024-01-12";
      // Assert
      assertEquals(statusResponse, result);
    }
  }

  @ParameterizedTest
  @MethodSource("argsProviderFactory")
  public void warnOnIllegalCombinations(
      DataService.PersonRegistrationStatus personRegistrationStatus, int warnTimes) {
    // Arrange
    final var mockRaskService = mock(RaskService.class);

    try (MockedStatic<LoggerSupplier> staticLoggerSupplier = mockStatic(LoggerSupplier.class)) {
      final LoggerSupplier mockLoggerSupplier = mock(LoggerSupplier.class);
      staticLoggerSupplier
          .when(() -> LoggerSupplier.forClass(DataService.class))
          .thenReturn(mockLoggerSupplier);
      final Logger mockLogger = mock(Logger.class);
      when(mockLoggerSupplier.get()).thenReturn(mockLogger);

      // Act
      new DataService(mockRaskService).warnOnIllegalCombinations(personRegistrationStatus);

      // Assert
      verify(mockLogger, times(warnTimes)).warn(anyString(), any(), any(), any());
      Mockito.reset(mockLogger);
    }
  }
}
