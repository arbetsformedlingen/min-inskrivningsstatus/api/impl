package se.jobtechdev.mininskrivningsstatus.api.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import se.arbetsformedlingen.rask.api.DefaultApi;
import se.arbetsformedlingen.rask.model.ArbetsokandeProcessStatusDTOKomposit;
import se.arbetsformedlingen.rask.model.ArbetssokandeKompaktDTO;
import se.jobtechdev.mininskrivningsstatus.api.exception.ApiException;
import se.jobtechdev.mininskrivningsstatus.api.generated.model.ErrorResponse;
import se.jobtechdev.mininskrivningsstatus.api.util.ErrorResponseFactory;
import se.jobtechdev.mininskrivningsstatus.api.util.RaskApiFactory;
import se.jobtechdev.mininskrivningsstatus.api.util.UuidProvider;

public class RaskServiceTest {
  @Test
  public void getRegistrationStatus() {
    // Arrange
    final var mockRaskApiFactory = mock(RaskApiFactory.class);
    final var mockClient = mock(DefaultApi.class);
    when(mockRaskApiFactory.createClient()).thenReturn(mockClient);

    try (final var staticUuidProvider = mockStatic(UuidProvider.class)) {
      final var mockTrackingId = mock(UUID.class);
      when(mockTrackingId.toString()).thenReturn("example-tracking-id");
      staticUuidProvider.when(UuidProvider::uuid).thenReturn(mockTrackingId);

      final var mockArbetssokandeKompaktDTO = mock(ArbetssokandeKompaktDTO.class);
      when(mockArbetssokandeKompaktDTO.getSokandeId()).thenReturn(123L);
      when(mockArbetssokandeKompaktDTO.getPersonnummer()).thenReturn("example-personal-number");

      when(mockClient.sokArbetssokande(
              "example-tracking-id",
              "example-af-system-id",
              "example-personal-number",
              null,
              "example-client-id",
              "example-client-secret",
              null,
              null,
              "example-client-id",
              "example-client-secret",
              "example-af-end-user-id"))
          .thenReturn(List.of(mockArbetssokandeKompaktDTO));

      final var mockArbetsokandeProcessStatusDTOKomposit =
          mock(ArbetsokandeProcessStatusDTOKomposit.class);

      when(mockClient.hamtaArbetssokandeProcessStatus(
              Long.valueOf(123L).toString(),
              "example-af-system-id",
              "example-tracking-id",
              "example-client-id",
              "example-client-secret",
              null,
              "example-af-end-user-id",
              "example-client-id",
              null,
              "example-client-secret"))
          .thenReturn(mockArbetsokandeProcessStatusDTOKomposit);

      // Act
      final var raskService =
          new RaskService(
              "example-client-id",
              "example-client-secret",
              "example-af-system-id",
              "example-af-end-user-id",
              "example-af-environment",
              mockRaskApiFactory);

      final var result = raskService.getRegistrationStatus("example-personal-number");

      // Assert
      verify(mockClient, times(1))
          .sokArbetssokande(
              "example-tracking-id",
              "example-af-system-id",
              "example-personal-number",
              null,
              "example-client-id",
              "example-client-secret",
              null,
              null,
              "example-client-id",
              "example-client-secret",
              "example-af-end-user-id");
      verify(mockClient, times(1))
          .hamtaArbetssokandeProcessStatus(
              Long.valueOf(123L).toString(),
              "example-af-system-id",
              "example-tracking-id",
              "example-client-id",
              "example-client-secret",
              null,
              "example-af-end-user-id",
              "example-client-id",
              null,
              "example-client-secret");
      assertEquals(Optional.of(mockArbetsokandeProcessStatusDTOKomposit), result);
    }
  }

  @Test
  public void
      getRegistrationStatus_whenPersonalNumberDoesNotMatch_throwApiExceptionInternalServerError() {
    // Arrange
    final var mockRaskApiFactory = mock(RaskApiFactory.class);
    final var mockClient = mock(DefaultApi.class);
    when(mockRaskApiFactory.createClient()).thenReturn(mockClient);

    final var constructionApiExceptionArgs = new HashMap<ApiException, List<?>>();
    try (final var staticErrorResponseFactory = mockStatic(ErrorResponseFactory.class);
        final var constructionApiException =
            mockConstruction(
                ApiException.class,
                (mock, context) -> {
                  constructionApiExceptionArgs.put(mock, context.arguments());
                });
        final var staticUuidProvider = mockStatic(UuidProvider.class)) {
      final var mockErrorResponse = mock(ErrorResponse.class);
      staticErrorResponseFactory
          .when(() -> ErrorResponseFactory.createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR))
          .thenReturn(mockErrorResponse);

      final var mockTrackingId = mock(UUID.class);
      when(mockTrackingId.toString()).thenReturn("example-tracking-id");
      staticUuidProvider.when(UuidProvider::uuid).thenReturn(mockTrackingId);

      final var mockArbetssokandeKompaktDTO = mock(ArbetssokandeKompaktDTO.class);
      when(mockArbetssokandeKompaktDTO.getSokandeId()).thenReturn(123L);
      when(mockArbetssokandeKompaktDTO.getPersonnummer()).thenReturn("other-personal-number");

      when(mockClient.sokArbetssokande(
              "example-tracking-id",
              "example-af-system-id",
              "example-personal-number",
              null,
              "example-client-id",
              "example-client-secret",
              null,
              null,
              "example-client-id",
              "example-client-secret",
              "example-af-end-user-id"))
          .thenReturn(List.of(mockArbetssokandeKompaktDTO));

      final var mockArbetsokandeProcessStatusDTOKomposit =
          mock(ArbetsokandeProcessStatusDTOKomposit.class);

      // Act
      final var raskService =
          new RaskService(
              "example-client-id",
              "example-client-secret",
              "example-af-system-id",
              "example-af-end-user-id",
              "example-af-environment",
              mockRaskApiFactory);

      assertThrows(
          ApiException.class, () -> raskService.getRegistrationStatus("example-personal-number"));

      // Assert
      verify(mockClient, times(1))
          .sokArbetssokande(
              "example-tracking-id",
              "example-af-system-id",
              "example-personal-number",
              null,
              "example-client-id",
              "example-client-secret",
              null,
              null,
              "example-client-id",
              "example-client-secret",
              "example-af-end-user-id");
    }
  }

  @Test
  public void
      getRegistrationStatus_whenTooManyResultsAreFound_throwApiExceptionInternalServerError() {
    // Arrange
    final var mockRaskApiFactory = mock(RaskApiFactory.class);
    final var mockClient = mock(DefaultApi.class);
    when(mockRaskApiFactory.createClient()).thenReturn(mockClient);

    final var constructionApiExceptionArgs = new HashMap<ApiException, List<?>>();
    try (final var staticErrorResponseFactory = mockStatic(ErrorResponseFactory.class);
        final var constructionApiException =
            mockConstruction(
                ApiException.class,
                (mock, context) -> {
                  constructionApiExceptionArgs.put(mock, context.arguments());
                });
        final var staticUuidProvider = mockStatic(UuidProvider.class)) {
      final var mockErrorResponse = mock(ErrorResponse.class);
      staticErrorResponseFactory
          .when(() -> ErrorResponseFactory.createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR))
          .thenReturn(mockErrorResponse);

      final var mockTrackingId = mock(UUID.class);
      when(mockTrackingId.toString()).thenReturn("example-tracking-id");
      staticUuidProvider.when(UuidProvider::uuid).thenReturn(mockTrackingId);

      final var mockArbetssokandeKompaktDTO1 = mock(ArbetssokandeKompaktDTO.class);
      final var mockArbetssokandeKompaktDTO2 = mock(ArbetssokandeKompaktDTO.class);

      when(mockClient.sokArbetssokande(
              "example-tracking-id",
              "example-af-system-id",
              "example-personal-number",
              null,
              "example-client-id",
              "example-client-secret",
              null,
              null,
              "example-client-id",
              "example-client-secret",
              "example-af-end-user-id"))
          .thenReturn(List.of(mockArbetssokandeKompaktDTO1, mockArbetssokandeKompaktDTO2));

      final var mockArbetsokandeProcessStatusDTOKomposit =
          mock(ArbetsokandeProcessStatusDTOKomposit.class);

      // Act
      final var raskService =
          new RaskService(
              "example-client-id",
              "example-client-secret",
              "example-af-system-id",
              "example-af-end-user-id",
              "example-af-environment",
              mockRaskApiFactory);

      assertThrows(
          ApiException.class, () -> raskService.getRegistrationStatus("example-personal-number"));

      // Assert
      verify(mockClient, times(1))
          .sokArbetssokande(
              "example-tracking-id",
              "example-af-system-id",
              "example-personal-number",
              null,
              "example-client-id",
              "example-client-secret",
              null,
              null,
              "example-client-id",
              "example-client-secret",
              "example-af-end-user-id");
    }
  }

  @Test
  public void
      getRegistrationStatus_whenRaskClientApiExceptionIsThrownButNotStatus404_throwApiExceptionBadGateway() {
    // Arrange
    final var mockRaskApiFactory = mock(RaskApiFactory.class);
    final var mockClient = mock(DefaultApi.class);
    when(mockRaskApiFactory.createClient()).thenReturn(mockClient);

    final var constructionApiExceptionArgs = new HashMap<ApiException, List<?>>();
    try (final var staticErrorResponseFactory = mockStatic(ErrorResponseFactory.class);
        final var constructionApiException =
            mockConstruction(
                ApiException.class,
                (mock, context) -> {
                  constructionApiExceptionArgs.put(mock, context.arguments());
                });
        final var staticUuidProvider = mockStatic(UuidProvider.class)) {
      final var mockErrorResponse = mock(ErrorResponse.class);
      staticErrorResponseFactory
          .when(() -> ErrorResponseFactory.createErrorResponse(HttpStatus.BAD_GATEWAY))
          .thenReturn(mockErrorResponse);

      final var mockTrackingId = mock(UUID.class);
      when(mockTrackingId.toString()).thenReturn("example-tracking-id");
      staticUuidProvider.when(UuidProvider::uuid).thenReturn(mockTrackingId);

      final var mockRaskClientApiException =
          mock(se.arbetsformedlingen.rask.client.ApiException.class);
      when(mockRaskClientApiException.getCode()).thenReturn(500);

      when(mockClient.sokArbetssokande(
              "example-tracking-id",
              "example-af-system-id",
              "example-personal-number",
              null,
              "example-client-id",
              "example-client-secret",
              null,
              null,
              "example-client-id",
              "example-client-secret",
              "example-af-end-user-id"))
          .thenThrow(mockRaskClientApiException);

      // Act
      final var raskService =
          new RaskService(
              "example-client-id",
              "example-client-secret",
              "example-af-system-id",
              "example-af-end-user-id",
              "example-af-environment",
              mockRaskApiFactory);

      final var thrown =
          assertThrows(
              ApiException.class,
              () -> raskService.getRegistrationStatus("example-personal-number"));

      // Assert
      verify(mockClient, times(1))
          .sokArbetssokande(
              "example-tracking-id",
              "example-af-system-id",
              "example-personal-number",
              null,
              "example-client-id",
              "example-client-secret",
              null,
              null,
              "example-client-id",
              "example-client-secret",
              "example-af-end-user-id");
    }
  }

  @Test
  public void
      getRegistrationStatus_whenRaskClientApiExceptionIsThrownAndStatus404_returnOptionalEmpty() {
    // Arrange
    final var mockRaskApiFactory = mock(RaskApiFactory.class);
    final var mockClient = mock(DefaultApi.class);
    when(mockRaskApiFactory.createClient()).thenReturn(mockClient);

    final var constructionApiExceptionArgs = new HashMap<ApiException, List<?>>();
    try (final var staticErrorResponseFactory = mockStatic(ErrorResponseFactory.class);
        final var constructionApiException =
            mockConstruction(
                ApiException.class,
                (mock, context) -> {
                  constructionApiExceptionArgs.put(mock, context.arguments());
                });
        final var staticUuidProvider = mockStatic(UuidProvider.class)) {
      final var mockErrorResponse = mock(ErrorResponse.class);
      staticErrorResponseFactory
          .when(() -> ErrorResponseFactory.createErrorResponse(HttpStatus.BAD_GATEWAY))
          .thenReturn(mockErrorResponse);

      final var mockTrackingId = mock(UUID.class);
      when(mockTrackingId.toString()).thenReturn("example-tracking-id");
      staticUuidProvider.when(UuidProvider::uuid).thenReturn(mockTrackingId);

      final var mockRaskClientApiException =
          mock(se.arbetsformedlingen.rask.client.ApiException.class);
      when(mockRaskClientApiException.getCode()).thenReturn(404);

      when(mockClient.sokArbetssokande(
              "example-tracking-id",
              "example-af-system-id",
              "example-personal-number",
              null,
              "example-client-id",
              "example-client-secret",
              null,
              null,
              "example-client-id",
              "example-client-secret",
              "example-af-end-user-id"))
          .thenThrow(mockRaskClientApiException);

      // Act
      final var raskService =
          new RaskService(
              "example-client-id",
              "example-client-secret",
              "example-af-system-id",
              "example-af-end-user-id",
              "example-af-environment",
              mockRaskApiFactory);

      final var result = raskService.getRegistrationStatus("example-personal-number");

      // Assert
      verify(mockClient, times(1))
          .sokArbetssokande(
              "example-tracking-id",
              "example-af-system-id",
              "example-personal-number",
              null,
              "example-client-id",
              "example-client-secret",
              null,
              null,
              "example-client-id",
              "example-client-secret",
              "example-af-end-user-id");
      assertEquals(Optional.empty(), result);
    }
  }
}
