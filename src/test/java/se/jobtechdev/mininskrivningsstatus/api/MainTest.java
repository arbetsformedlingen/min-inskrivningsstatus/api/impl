package se.jobtechdev.mininskrivningsstatus.api;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.springframework.boot.SpringApplication;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;

public class MainTest {
    @Test
    @Tag("UnitTest")
    public void construct() {
        new Main();
    }

    @Test
    public void main() {
        try (MockedStatic<SpringApplication> springApplicationMockedStatic = mockStatic(SpringApplication.class)) {
            final String[] args = new String[] {};

            Main.main(args);

            springApplicationMockedStatic.verify(
                    () -> SpringApplication.run(Main.class, args),
                    times(1)
            );
        }
    }

    @Test
    public void onApplicationReadyEvent() {
        // Act and Assert
        final var main = new Main();

        assertFalse(Main.isStarted());

        main.onApplicationReadyEvent();

        assertTrue(Main.isStarted());
    }
}
