package se.jobtechdev.mininskrivningsstatus.api.exception;

import org.junit.jupiter.api.Test;
import se.jobtechdev.mininskrivningsstatus.api.generated.model.ErrorResponse;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

public class ApiExceptionTest {
    @Test
    public void construct() {
        final var mockErrorResponse = mock(ErrorResponse.class);
        final var apiException = new ApiException(mockErrorResponse);
        assertEquals(mockErrorResponse, apiException.getErrorResponse());
    }
}
