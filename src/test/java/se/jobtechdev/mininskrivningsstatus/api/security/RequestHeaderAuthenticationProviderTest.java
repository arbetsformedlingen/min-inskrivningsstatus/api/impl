package se.jobtechdev.mininskrivningsstatus.api.security;

import org.junit.jupiter.api.Test;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class RequestHeaderAuthenticationProviderTest {
    @Test
    public void construct() {
        new RequestHeaderAuthenticationProvider("example-client-auth-key");
    }

    @Test
    public void verifyAuthKey_whenAuthKeyIsNull_throwBadCredentialsException() {
        // Act
        final var provider = new RequestHeaderAuthenticationProvider("example-client-auth-key");
        final var thrown = assertThrows(BadCredentialsException.class, () -> provider.verifyAuthKey(null));

        // Assert
        assertEquals("Bad Request Header Credentials, provided authKey is null", thrown.getMessage());
    }

    @Test
    public void verifyAuthKey_whenAuthKeyIsEmpty_throwBadCredentialsException() {
        // Act
        final var provider = new RequestHeaderAuthenticationProvider("example-client-auth-key");
        final var thrown = assertThrows(BadCredentialsException.class, () -> provider.verifyAuthKey(""));

        // Assert
        assertEquals("Bad Request Header Credentials, provided authKey is empty", thrown.getMessage());
    }

    @Test
    public void verifyAuthKey_whenAuthKeyDoesNotMatchConfiguredClientAuthKey_throwBadCredentialsException() {
        // Act
        final var provider = new RequestHeaderAuthenticationProvider("example-client-auth-key");
        final var thrown = assertThrows(BadCredentialsException.class, () -> provider.verifyAuthKey("incorrect-client-auth-key"));

        // Assert
        assertEquals("Bad Request Header Credentials, provided authKey does not match", thrown.getMessage());
    }

    @Test
    public void verifyAuthKey_whenAllChecksPass_returnTrue() {
        // Act
        final var provider = new RequestHeaderAuthenticationProvider("example-client-auth-key");
        final var result = provider.verifyAuthKey("example-client-auth-key");

        // Assert
        assertTrue(result);
    }

    @Test
    public void authenticate() {
        // Arrange
        final var mockAuthentication = mock(Authentication.class);
        when(mockAuthentication.getPrincipal()).thenReturn("example-client-auth-key");

        final var constructionSimpleGrantedAuthorityArgs = new HashMap<SimpleGrantedAuthority, List<?>>();
        final var constructionPreAuthenticatedAuthenticationTokenArgs = new HashMap<PreAuthenticatedAuthenticationToken, List<?>>();
        try (final var constructionSimpleGrantedAuthority = mockConstruction(SimpleGrantedAuthority.class, (mock, context) -> {
            constructionSimpleGrantedAuthorityArgs.put(mock, context.arguments());
        });
             final var constructionPreAuthenticatedAuthenticationToken = mockConstruction(PreAuthenticatedAuthenticationToken.class, (mock, context) -> {
                 constructionPreAuthenticatedAuthenticationTokenArgs.put(mock, context.arguments());
             })) {
            // Act
            final var provider = new RequestHeaderAuthenticationProvider("example-client-auth-key");
            final var result = provider.authenticate(mockAuthentication);

            // Assert
            assertEquals(1, constructionSimpleGrantedAuthority.constructed().size());
            final var mockSimpleGrantedAuthority = constructionSimpleGrantedAuthority.constructed().get(0);
            assertEquals("client", constructionSimpleGrantedAuthorityArgs.get(mockSimpleGrantedAuthority).get(0));
            assertEquals(1, constructionPreAuthenticatedAuthenticationToken.constructed().size());
            final var mockPreAuthenticatedAuthenticationToken = constructionPreAuthenticatedAuthenticationToken.constructed().get(0);
            assertEquals("example-client-auth-key", constructionPreAuthenticatedAuthenticationTokenArgs.get(mockPreAuthenticatedAuthenticationToken).get(0));
            assertNull(constructionPreAuthenticatedAuthenticationTokenArgs.get(mockPreAuthenticatedAuthenticationToken).get(1));
            assertEquals(List.of(mockSimpleGrantedAuthority), constructionPreAuthenticatedAuthenticationTokenArgs.get(mockPreAuthenticatedAuthenticationToken).get(2));
            assertEquals(mockPreAuthenticatedAuthenticationToken, result);
        }
    }

    @Test
    public void supports() {
        // Arrange
        final var mockAuthentication = mock(PreAuthenticatedAuthenticationToken.class);

        // Act
        final var provider = new RequestHeaderAuthenticationProvider("example-client-auth-key");
        final var result = provider.supports(mockAuthentication.getClass());

        // Assert
        assertTrue(result);
    }

    @Test
    public void supports_whenClassDoesNotEqualPreAuthenticatedAuthenticationTokenClass_thenReturnFalse() {
        // Arrange
        final var mockAuthentication = mock(Object.class);

        // Act
        final var provider = new RequestHeaderAuthenticationProvider("example-client-auth-key");
        final var result = provider.supports(mockAuthentication.getClass());

        // Assert
        assertFalse(result);
    }
}
