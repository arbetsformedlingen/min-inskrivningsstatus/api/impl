package se.jobtechdev.mininskrivningsstatus.api.controller;

import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import se.jobtechdev.mininskrivningsstatus.api.exception.ApiException;
import se.jobtechdev.mininskrivningsstatus.api.generated.model.ErrorResponse;
import se.jobtechdev.mininskrivningsstatus.api.generated.model.GetPersonRegistrationStatusResponse;
import se.jobtechdev.mininskrivningsstatus.api.service.DataService;
import se.jobtechdev.mininskrivningsstatus.api.util.ErrorResponseFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PersonsControllerTest {
    private PersonsController personsController;

    @Mock
    private NativeWebRequest request;

    @Mock
    private DataService dataService;

    @BeforeEach
    void setUp() {
        personsController = new PersonsController(request, dataService);
    }

    @Test
    public void getPersonRegistrationStatus_whenMissingAcceptHeader_throwApiException() {
        // Arrange
        try (final var staticResponseEntity = mockStatic(ResponseEntity.class);
             final var staticErrorResponseFactory = mockStatic(ErrorResponseFactory.class)) {
            final var mockGetPersonRegistrationStatusResponse = mock(GetPersonRegistrationStatusResponse.class);
            final var mockResponseEntity = mock(ResponseEntity.class);

            when(request.getHeader(HttpHeaders.ACCEPT)).thenReturn(null);
            staticResponseEntity.when(() -> ResponseEntity.ok(mockGetPersonRegistrationStatusResponse)).thenReturn(mockResponseEntity);
            final var mockErrorResponse = mock(ErrorResponse.class);
            when(ErrorResponseFactory.createErrorResponse(HttpStatus.NOT_ACCEPTABLE)).thenReturn(mockErrorResponse);

            // Act, Assert
            final var thrown = assertThrows(ApiException.class, () -> personsController.getPersonRegistrationStatus("example-person-id"));

            assertEquals(mockErrorResponse, thrown.getErrorResponse());
        }
    }

    @Test
    public void getPersonRegistrationStatus_whenAcceptHeaderIsApplicationJson_returnGetPersonRegistrationStatusResponse() {
        // Arrange
        try (final var staticResponseEntity = mockStatic(ResponseEntity.class)) {
            final var mockGetPersonRegistrationStatusResponse = mock(GetPersonRegistrationStatusResponse.class);
            final var mockResponseEntity = mock(ResponseEntity.class);
            final var mockStatus = mock(DataService.PersonRegistrationStatus.class);

            when(request.getHeader(HttpHeaders.ACCEPT)).thenReturn("application/json");
            when(dataService.getPersonRegistrationStatus("example-person-id")).thenReturn(mockStatus);
            when(dataService.createJsonResponse(mockStatus)).thenReturn(mockGetPersonRegistrationStatusResponse);
            staticResponseEntity.when(() -> ResponseEntity.ok(mockGetPersonRegistrationStatusResponse)).thenReturn(mockResponseEntity);

            // Act
            final var result = personsController.getPersonRegistrationStatus("example-person-id");

            // Assert
            verify(dataService, times(1)).getPersonRegistrationStatus("example-person-id");
            verify(dataService, times(1)).createJsonResponse(mockStatus);
            staticResponseEntity.verify(() -> ResponseEntity.ok(mockGetPersonRegistrationStatusResponse), times(1));
            assertEquals(mockResponseEntity, result);
        }
    }

    @Test
    public void getPersonRegistrationStatus_whenAcceptHeaderIsTextPlain_writeText() throws IOException {
        final var args = new HashMap<ResponseEntity<?>, List<?>>();
        // Arrange
        try (final var responseEntityConstruction = mockConstruction(ResponseEntity.class, (mock, context) -> {
            args.put(mock, context.arguments());
        })) {
            final var mockHttpServletResponse = mock(HttpServletResponse.class);
            when(request.getNativeResponse(HttpServletResponse.class)).thenReturn(mockHttpServletResponse);
            final var mockWriter = mock(PrintWriter.class);
            when(mockHttpServletResponse.getWriter()).thenReturn(mockWriter);
            final var mockStatus = mock(DataService.PersonRegistrationStatus.class);

            when(request.getHeader(HttpHeaders.ACCEPT)).thenReturn("text/plain");
            when(dataService.getPersonRegistrationStatus("example-person-id")).thenReturn(mockStatus);
            when(dataService.createTextResponse(mockStatus)).thenReturn("response string");

            // Act
            final var result = personsController.getPersonRegistrationStatus("example-person-id");

            // Assert
            verify(mockHttpServletResponse, times(1)).setCharacterEncoding("UTF-8");
            verify(mockHttpServletResponse, times(1)).addHeader("Content-Type", "text/plain");
            verify(mockWriter, times(1)).print("response string");
            verify(dataService, times(1)).createTextResponse(mockStatus);
            final var responseEntity = responseEntityConstruction.constructed().get(0);
            assertEquals(args.get(responseEntity), List.of(HttpStatus.OK));
            assertEquals(responseEntity, result);
        }
    }

    @Test
    public void getPersonRegistrationStatus_whenAcceptHeaderCanNotBeHandled_throwApiException() {
        // Arrange
        try (final var staticResponseEntity = mockStatic(ResponseEntity.class);
             final var staticErrorResponseFactory = mockStatic(ErrorResponseFactory.class)
        ) {
            final var mockGetPersonRegistrationStatusResponse = mock(GetPersonRegistrationStatusResponse.class);
            final var mockResponseEntity = mock(ResponseEntity.class);

            when(request.getHeader(HttpHeaders.ACCEPT)).thenReturn("image/png");
            staticResponseEntity.when(() -> ResponseEntity.ok(mockGetPersonRegistrationStatusResponse)).thenReturn(mockResponseEntity);
            final var mockErrorResponse = mock(ErrorResponse.class);
            when(ErrorResponseFactory.createErrorResponse(HttpStatus.NOT_ACCEPTABLE)).thenReturn(mockErrorResponse);
            when(ErrorResponseFactory.createErrorResponse(eq(HttpStatus.NOT_ACCEPTABLE), anyString())).thenReturn(mockErrorResponse);

            // Act, Assert
            final var thrown = assertThrows(ApiException.class, () -> personsController.getPersonRegistrationStatus("example-person-id"));

            assertEquals(mockErrorResponse, thrown.getErrorResponse());
        }
    }

    @Test
    public void getPersonRegistrationStatus_whenWriterThrowsIOException_throwApiException() {
        final var args = new HashMap<ResponseEntity<?>, List<?>>();
        // Arrange
        try (final var staticErrorResponseFactory = mockStatic(ErrorResponseFactory.class);
             final var responseEntityConstruction = mockConstruction(ResponseEntity.class, (mock, context) -> {
            args.put(mock, context.arguments());
        })) {
            final var mockHttpServletResponse = mock(HttpServletResponse.class);
            when(request.getNativeResponse(HttpServletResponse.class)).thenReturn(mockHttpServletResponse);
            when(mockHttpServletResponse.getWriter()).thenThrow(new IOException());
            final var mockStatus = mock(DataService.PersonRegistrationStatus.class);

            when(request.getHeader(HttpHeaders.ACCEPT)).thenReturn("text/plain");
            when(dataService.getPersonRegistrationStatus("example-person-id")).thenReturn(mockStatus);
            when(dataService.createTextResponse(mockStatus)).thenReturn("response string");
            final var mockErrorResponse = mock(ErrorResponse.class);
            when(ErrorResponseFactory.createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR)).thenReturn(mockErrorResponse);

            // Act, Assert
            final var thrown = assertThrows(ApiException.class, () -> personsController.getPersonRegistrationStatus("example-person-id"));
            assertEquals(mockErrorResponse, thrown.getErrorResponse());

        } catch (IOException e) {
            fail("should not happen");
        }
    }
}
