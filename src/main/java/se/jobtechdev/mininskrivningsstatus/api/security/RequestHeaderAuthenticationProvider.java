package se.jobtechdev.mininskrivningsstatus.api.security;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;
import se.jobtechdev.mininskrivningsstatus.api.util.LoggerSupplier;

import java.util.List;

@Component
public class RequestHeaderAuthenticationProvider implements AuthenticationProvider {

    private final Logger logger = LoggerSupplier.forClass(RequestHeaderAuthenticationProvider.class).get();

    private final String clientAuthKey;

    @Autowired
    public RequestHeaderAuthenticationProvider(
            @Value("${mi-api.client-auth-key}") String clientAuthKey
    ) {
        this.clientAuthKey = clientAuthKey;
    }

    public boolean verifyAuthKey(String authKey) {
        if (authKey == null) throw new BadCredentialsException("Bad Request Header Credentials, provided authKey is null");
        if (authKey.isEmpty()) throw new BadCredentialsException("Bad Request Header Credentials, provided authKey is empty");
        if (!authKey.equals(clientAuthKey)) throw new BadCredentialsException("Bad Request Header Credentials, provided authKey does not match");
        return true;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        final var authKey = String.valueOf(authentication.getPrincipal());

        verifyAuthKey(authKey);

        final var simpleGrantedAuthority = new SimpleGrantedAuthority("client");

        return new PreAuthenticatedAuthenticationToken(authentication.getPrincipal(), null, List.of(simpleGrantedAuthority));
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(PreAuthenticatedAuthenticationToken.class);
    }
}