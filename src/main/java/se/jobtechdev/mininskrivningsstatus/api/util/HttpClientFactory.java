package se.jobtechdev.mininskrivningsstatus.api.util;

import javax.annotation.Nullable;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.HttpHost;

public class HttpClientFactory {
  private HttpClientFactory() {}

  public static CloseableHttpClient createHttpClient(
      @Nullable ProxyConfiguration proxyConfiguration) {
    final var httpClientBuilder = HttpClients.custom();

    if (proxyConfiguration != null) {
      httpClientBuilder.setProxy(
          new HttpHost(
              proxyConfiguration.protocol(), proxyConfiguration.host(), proxyConfiguration.port()));
    }

    return httpClientBuilder.build();
  }

  public record ProxyConfiguration(String protocol, String host, int port) {}
}
