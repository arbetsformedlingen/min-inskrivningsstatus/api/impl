package se.jobtechdev.mininskrivningsstatus.api.util;

import java.util.UUID;

public class UuidProvider {
    private UuidProvider() {}

    public static UUID uuid() {
        return UUID.randomUUID();
    }
}
