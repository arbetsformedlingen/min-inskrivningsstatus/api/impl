package se.jobtechdev.mininskrivningsstatus.api.util;

import org.springframework.http.ResponseEntity;
import se.jobtechdev.mininskrivningsstatus.api.generated.model.ErrorResponse;

public class ResponseEntityFactory {
    public static ResponseEntity<?> create(ErrorResponse errorResponse) {
        return ResponseEntity.status(errorResponse.getStatus()).body(errorResponse);
    }
}
