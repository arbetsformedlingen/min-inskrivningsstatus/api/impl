package se.jobtechdev.mininskrivningsstatus.api.util;

import java.time.ZonedDateTime;

public class TimeProvider {
    private TimeProvider() {

    }

    public static Long currentTimeMillis() {
        return System.currentTimeMillis();
    }

    public static ZonedDateTime now() {
        return ZonedDateTime.now();
    }
}
