package se.jobtechdev.mininskrivningsstatus.api.util;

import se.jobtechdev.mininskrivningsstatus.api.generated.model.*;

import java.time.LocalDate;

public class ResponseFactory {
  private ResponseFactory() {
  }

  public static GetApiInfoResponse getApiInfoResponse(
      String apiName,
      String apiVersion,
      String buildVersion,
      LocalDate apiReleased,
      java.net.URL apiDocumentation,
      ApiStatus apiStatus) {
    return new GetApiInfoResponse(
        apiName, apiVersion, buildVersion, apiReleased, apiDocumentation, apiStatus);
  }

  public static GetHealthResponse getHealthResponse(Status status, HealthChecks checks) {
    return new GetHealthResponse(status, checks);
  }
}
