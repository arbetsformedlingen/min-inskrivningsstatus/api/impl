package se.jobtechdev.mininskrivningsstatus.api.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.rask.api.DefaultApi;
import se.arbetsformedlingen.rask.client.ApiClient;

@Component
public class RaskApiFactory {
  private final String baseUrl;
  private HttpClientFactory.ProxyConfiguration proxyConfiguration;
  private DefaultApi client;

  public RaskApiFactory(
      @Value("${mi-api.rask.base-url}") String baseUrl,
      @Value("${mi-api.rask.proxy.protocol:#{null}}") String proxyProtocol,
      @Value("${mi-api.rask.proxy.host:#{null}}") String proxyHost,
      @Value("${mi-api.rask.proxy.port:#{null}}") Integer proxyPort) {
    this.baseUrl = baseUrl;

    if (proxyProtocol != null && proxyHost != null && proxyPort != null) {
      proxyConfiguration =
          new HttpClientFactory.ProxyConfiguration(proxyProtocol, proxyHost, proxyPort);
    }
  }

  public DefaultApi createClient() {
    if (client == null) {
      final var httpClient = HttpClientFactory.createHttpClient(proxyConfiguration);
      final var apiClient = new ApiClient(httpClient);
      apiClient.setBasePath(baseUrl);
      client = new DefaultApi(apiClient);
    }
    return client;
  }
}
