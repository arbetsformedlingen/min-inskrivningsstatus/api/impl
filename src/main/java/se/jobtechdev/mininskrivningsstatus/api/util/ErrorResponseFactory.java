package se.jobtechdev.mininskrivningsstatus.api.util;

import se.jobtechdev.mininskrivningsstatus.api.generated.model.ErrorResponse;
import org.springframework.http.HttpStatus;

public class ErrorResponseFactory {
    public static ErrorResponse createErrorResponse(HttpStatus httpStatus) {
        return new ErrorResponse(httpStatus.value(), httpStatus.getReasonPhrase());
    }

    public static ErrorResponse createErrorResponse(HttpStatus httpStatus, String message) {
        return createErrorResponse(httpStatus).message(message);
    }
}
