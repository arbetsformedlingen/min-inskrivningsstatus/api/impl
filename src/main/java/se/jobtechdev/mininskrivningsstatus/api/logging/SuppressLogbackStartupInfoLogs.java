package se.jobtechdev.mininskrivningsstatus.api.logging;

import ch.qos.logback.core.status.OnConsoleStatusListener;
import ch.qos.logback.core.status.Status;
import java.util.List;

public class SuppressLogbackStartupInfoLogs extends OnConsoleStatusListener {
    protected void hookPostAddStatusEvent(Status status) {

    }

    protected void hookPostStart(Status status) {

    }

    @Override
    public void addStatusEvent(Status status) {
        if (status.getLevel() == Status.INFO) return;
        super.addStatusEvent(status);
        hookPostAddStatusEvent(status);
    }

    @Override
    public void start() {
        final List<Status> statuses = context.getStatusManager().getCopyOfStatusList();
        for (Status status : statuses) {
            if (status.getLevel() != Status.INFO) {
                super.start();
                hookPostStart(status);
            }
        }
    }
}
