package se.jobtechdev.mininskrivningsstatus.api.logging;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogMasker {
    private static final String[] masks = {
            "\\\\\"personalIdentityNumber\\\\\":\\\\\"([0-9\\-]+)\\\\\"",
            "\\\"personalIdentityNumber\\\":\\\"([0-9\\-]+)\\\""
    };

    private static final Pattern maskPattern = Pattern.compile(String.join("|", masks),
            Pattern.MULTILINE | Pattern.CASE_INSENSITIVE
    );

    public static String mask(String original) {
        Matcher matcher = maskPattern.matcher(original);

        StringBuilder result = new StringBuilder(original);
        // as we replace text, the whole block shifts and we need to take it into account for the next replaces
        int offset = 0;
        while (matcher.find()) {
            for (int groupIndex=1; groupIndex<=matcher.groupCount(); groupIndex++) {
                String toMask = matcher.group(groupIndex);
                if (toMask != null) {
                    int start = matcher.start(groupIndex) - offset, end = matcher.end(groupIndex) - offset;
                    String masked = "REDACTED";
                    result.replace(start, end, masked);
                    offset += toMask.length() - masked.length();
                }
            }
        }
        return result.toString();
    }
}
