package se.jobtechdev.mininskrivningsstatus.api.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import se.jobtechdev.mininskrivningsstatus.api.exception.ApiException;
import se.jobtechdev.mininskrivningsstatus.api.util.ErrorResponseFactory;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import se.jobtechdev.mininskrivningsstatus.api.generated.model.ErrorResponse;
import se.jobtechdev.mininskrivningsstatus.api.util.ResponseEntityFactory;

@RestController
@RestControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler implements ErrorController {
    private static final String ERROR_PATH = "/error";

    @ExceptionHandler(ApiException.class)
    public ResponseEntity<?> exception(HttpServletRequest request, ApiException e) {
        final var objectMapper = new ObjectMapper();
        try {
            final var errorResponse = e.getErrorResponse();
            return ResponseEntity.status(errorResponse.getStatus()).contentType(MediaType.APPLICATION_JSON).body(objectMapper.writeValueAsString(errorResponse));
        } catch (JsonProcessingException ex) {
            return ResponseEntity.status(500).contentType(MediaType.APPLICATION_JSON).body("{\"status\":500,\"error\":\"Internal Server Error\",\"message\":\"Unexpected exception occurred\"}");
        }
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> exception(HttpServletRequest request, Exception e) {
        final ErrorResponse errorResponse = ErrorResponseFactory.createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Unexpected exception occurred");
        return ResponseEntityFactory.create(errorResponse);
    }

    @RequestMapping(path = ERROR_PATH)
    public ResponseEntity<?> errorHandler() {
        final ErrorResponse errorResponse = ErrorResponseFactory.createErrorResponse(HttpStatus.NOT_FOUND, "Not found");
        return ResponseEntityFactory.create(errorResponse);
    }
}
