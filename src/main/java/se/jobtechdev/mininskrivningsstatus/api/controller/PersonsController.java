package se.jobtechdev.mininskrivningsstatus.api.controller;

import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.context.request.NativeWebRequest;
import se.jobtechdev.mininskrivningsstatus.api.exception.ApiException;
import se.jobtechdev.mininskrivningsstatus.api.generated.api.PersonsApi;
import se.jobtechdev.mininskrivningsstatus.api.generated.model.GetPersonRegistrationStatusResponse;
import se.jobtechdev.mininskrivningsstatus.api.service.DataService;
import se.jobtechdev.mininskrivningsstatus.api.util.ErrorResponseFactory;

import java.io.IOException;
import java.util.Objects;
import java.util.Set;

@CrossOrigin("*")
@Controller
public class PersonsController implements PersonsApi {
    private final NativeWebRequest request;
    private final DataService dataService;

    @Autowired
    public PersonsController(
            NativeWebRequest request,
            DataService dataService
    ) {
        this.request = request;
        this.dataService = dataService;
    }

    @Override
    public ResponseEntity<GetPersonRegistrationStatusResponse> getPersonRegistrationStatus(String personId) {
        final var httpAcceptHeader = request.getHeader(HttpHeaders.ACCEPT);
        if (httpAcceptHeader == null)
            throw new ApiException(ErrorResponseFactory.createErrorResponse(HttpStatus.NOT_ACCEPTABLE));

        final var acceptHeaders = httpAcceptHeader.split(",");
        for (final var acceptHeader : acceptHeaders) {
            if (acceptHeader.equals(MimeTypeUtils.APPLICATION_JSON_VALUE)) {
                final var status = dataService.getPersonRegistrationStatus(personId);
                return ResponseEntity.ok(dataService.createJsonResponse(status));
            } else if (acceptHeader.equals(MimeTypeUtils.TEXT_PLAIN_VALUE)) {
                final var status = dataService.getPersonRegistrationStatus(personId);
                createResponse(request, acceptHeader, dataService.createTextResponse(status));
                return new ResponseEntity<>(HttpStatus.OK);
            }
        }
        final var errorMessage = String.format("%s not acceptable, acceptable representations: [application/json, text/plain].", httpAcceptHeader);
        throw new ApiException(ErrorResponseFactory.createErrorResponse(HttpStatus.NOT_ACCEPTABLE, errorMessage));
    }

    public static void createResponse(NativeWebRequest req, String contentType, String body) {
        try {
            HttpServletResponse res = req.getNativeResponse(HttpServletResponse.class);
            Objects.requireNonNull(res).setCharacterEncoding("UTF-8");
            res.addHeader("Content-Type", contentType);
            res.getWriter().print(body);
        } catch (IOException e) {
            throw new ApiException(ErrorResponseFactory.createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR));
        }
    }
}
