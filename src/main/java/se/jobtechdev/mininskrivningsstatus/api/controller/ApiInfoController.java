package se.jobtechdev.mininskrivningsstatus.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import se.jobtechdev.mininskrivningsstatus.api.generated.api.ApiInfoApi;
import se.jobtechdev.mininskrivningsstatus.api.generated.model.ApiStatus;
import se.jobtechdev.mininskrivningsstatus.api.generated.model.GetApiInfoResponse;
import se.jobtechdev.mininskrivningsstatus.api.util.ResponseFactory;
import se.jobtechdev.mininskrivningsstatus.api.util.UrlFactory;

import java.net.URL;
import java.time.LocalDate;

@Controller
public class ApiInfoController implements ApiInfoApi {
  private static final Logger logger = LoggerFactory.getLogger(ApiInfoController.class);

  private final String apiName;
  private final String apiVersion;
  private final URL apiDocumentation;
  private final LocalDate apiReleased;
  private final ApiStatus apiStatus;
  private final String buildVersion;

  @Autowired
  public ApiInfoController(
      @Value("${app.project.name}") String apiName,
      @Value("${api.spec.version}") String apiVersion,
      @Value("${app.build.version}") String buildVersion,
      @Value("${app.project.url}") String apiDocumentation,
      @Value("${mi-api.released}") String released,
      @Value("${mi-api.status:#{'active'}}") String status) {
    logger.info(
        "apiName = {}, apiVersion = {}, apiDocumentation = {}, released = {}, status = {}",
        apiName,
        apiVersion,
        apiDocumentation,
        released,
        status);
    this.apiName = apiName;
    this.apiVersion = apiVersion;
    this.buildVersion = buildVersion;
    this.apiDocumentation = UrlFactory.createURL(apiDocumentation);
    this.apiReleased = LocalDate.parse(released);
    this.apiStatus = ApiStatus.fromValue(status);
  }

  @Override
  public ResponseEntity<GetApiInfoResponse> apiInfo() {
    final var response = ResponseFactory.getApiInfoResponse(
        apiName, apiVersion, buildVersion, apiReleased, apiDocumentation, apiStatus);

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .body(response);
  }
}
