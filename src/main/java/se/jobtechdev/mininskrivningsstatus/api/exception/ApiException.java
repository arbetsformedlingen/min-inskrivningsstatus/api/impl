package se.jobtechdev.mininskrivningsstatus.api.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import se.jobtechdev.mininskrivningsstatus.api.generated.model.ErrorResponse;

@Getter
@AllArgsConstructor
public class ApiException extends RuntimeException {
    private final ErrorResponse errorResponse;
}
