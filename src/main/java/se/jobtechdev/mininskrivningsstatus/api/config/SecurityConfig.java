package se.jobtechdev.mininskrivningsstatus.api.config;

import java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.AuthorizeHttpRequestsConfigurer;
import org.springframework.security.config.annotation.web.configurers.SessionManagementConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;
import org.springframework.security.web.header.HeaderWriterFilter;
import org.springframework.security.web.util.matcher.*;
import se.jobtechdev.mininskrivningsstatus.api.security.RequestHeaderAuthenticationProvider;

@Configuration
@EnableWebSecurity
public class SecurityConfig {
  private final RequestHeaderAuthenticationProvider requestHeaderAuthenticationProvider;

  @Autowired
  public SecurityConfig(RequestHeaderAuthenticationProvider requestHeaderAuthenticationProvider) {
    this.requestHeaderAuthenticationProvider = requestHeaderAuthenticationProvider;
  }

  public Customizer<
          AuthorizeHttpRequestsConfigurer<HttpSecurity>.AuthorizationManagerRequestMatcherRegistry>
      getAuthorizeHttpRequestsCustomizer() {
    return registry -> {
      registry.requestMatchers(HttpMethod.GET, "/error").permitAll();
      registry.requestMatchers(HttpMethod.GET, "/health").permitAll();
      registry.requestMatchers(HttpMethod.GET, "/api-info").permitAll();
      registry
          .requestMatchers(HttpMethod.GET, "/persons/*/registrationStatus")
          .hasAnyAuthority("client");
    };
  }

  @Bean
  public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
    return http.cors(Customizer.withDefaults())
        .csrf(AbstractHttpConfigurer::disable)
        .sessionManagement(getSessionManagementConfigurerCustomizer())
        .addFilterBefore(requestHeaderAuthenticationFilter(), HeaderWriterFilter.class)
        .authorizeHttpRequests(getAuthorizeHttpRequestsCustomizer())
        .build();
  }

  public Customizer<SessionManagementConfigurer<HttpSecurity>>
      getSessionManagementConfigurerCustomizer() {
    return session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
  }

  @Bean
  public RequestHeaderAuthenticationFilter requestHeaderAuthenticationFilter() {
    RequestHeaderAuthenticationFilter filter = new RequestHeaderAuthenticationFilter();
    filter.setPrincipalRequestHeader("X-Auth-Key");
    filter.setRequiresAuthenticationRequestMatcher(
        new AntPathRequestMatcher("/persons/*/registrationStatus"));
    filter.setAuthenticationManager(authenticationManager());

    return filter;
  }

  @Bean
  protected AuthenticationManager authenticationManager() {
    return new ProviderManager(Collections.singletonList(requestHeaderAuthenticationProvider));
  }
}
