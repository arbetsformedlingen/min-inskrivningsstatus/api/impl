package se.jobtechdev.mininskrivningsstatus.api.service;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.rask.model.ArbetsokandeProcessStatusDTOKomposit;
import se.arbetsformedlingen.rask.model.ArbetssokandeKompaktDTO;
import se.jobtechdev.mininskrivningsstatus.api.exception.ApiException;
import se.jobtechdev.mininskrivningsstatus.api.util.ErrorResponseFactory;
import se.jobtechdev.mininskrivningsstatus.api.util.LoggerSupplier;
import se.jobtechdev.mininskrivningsstatus.api.util.RaskApiFactory;
import se.jobtechdev.mininskrivningsstatus.api.util.UuidProvider;

@Component
public class RaskService {
  private final Logger logger = LoggerSupplier.forClass(RaskService.class).get();
  private final RaskApiFactory raskApiFactory;
  private final String clientId;
  private final String clientSecret;
  private final String afSystemId;
  private final String afEndUserId;
  private final String afEnvironment;

  public RaskService(
      @Value("${mi-api.rask.client-id}") String clientId,
      @Value("${mi-api.rask.client-secret}") String clientSecret,
      @Value("${mi-api.rask.af-system-id}") String afSystemId,
      @Value("${mi-api.rask.af-end-user-id}") String afEndUserId,
      @Value("${mi-api.rask.af-environment}") String afEnvironment,
      @Autowired RaskApiFactory raskApiFactory) {
    this.clientId = clientId;
    this.clientSecret = clientSecret;
    this.afSystemId = afSystemId;
    this.afEndUserId = afEndUserId;
    this.afEnvironment = afEnvironment;
    this.raskApiFactory = raskApiFactory;
  }

  public Optional<ArbetsokandeProcessStatusDTOKomposit> getRegistrationStatus(
      String personalNumber) {
    final var client = raskApiFactory.createClient();

    final var trackingId = UuidProvider.uuid();

    final List<ArbetssokandeKompaktDTO> arbetssokande;

    try {
      arbetssokande =
          client.sokArbetssokande(
              trackingId.toString(),
              afSystemId,
              personalNumber,
              null,
              clientId,
              clientSecret,
              null,
              null,
              clientId,
              clientSecret,
              afEndUserId);
    } catch (se.arbetsformedlingen.rask.client.ApiException raskClientApiException) {
      if (raskClientApiException.getCode() == 404) {
        return Optional.empty();
      }
      logger.error(
          "Failed to query Rask sokArbetssokande-endpoint! Rask Api client threw error",
          raskClientApiException);
      throw new ApiException(ErrorResponseFactory.createErrorResponse(HttpStatus.BAD_GATEWAY));
    }

    if (arbetssokande.size() > 1) {
      logger.error(
          "Received too many process statuses, expected at most one result, but received {} results",
          arbetssokande.size());
      throw new ApiException(
          ErrorResponseFactory.createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    final var result = arbetssokande.get(0);
    final var sokandeId = result.getSokandeId().toString();
    final var personnummer = result.getPersonnummer();

    if (!personalNumber.equals(personnummer)) {
      logger.error(
          "Detected data inconsistency! Received unexpected personal number from Rask sokArbetssokande-endpoint!");
      throw new ApiException(
          ErrorResponseFactory.createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    final var processStatus =
        client.hamtaArbetssokandeProcessStatus(
            sokandeId,
            afSystemId,
            trackingId.toString(),
            clientId,
            clientSecret,
            null,
            afEndUserId,
            clientId,
            null,
            clientSecret);

    return Optional.of(processStatus);
  }
}
