package se.jobtechdev.mininskrivningsstatus.api.service;

import java.time.LocalDate;
import java.time.ZoneOffset;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import se.arbetsformedlingen.rask.model.ArbetsokandeProcessStatusDTOKompositProcessStatus;
import se.jobtechdev.mininskrivningsstatus.api.exception.ApiException;
import se.jobtechdev.mininskrivningsstatus.api.generated.model.GetPersonRegistrationStatusResponse;
import se.jobtechdev.mininskrivningsstatus.api.util.ErrorResponseFactory;
import se.jobtechdev.mininskrivningsstatus.api.util.LoggerSupplier;

@Component
public class DataService {

  private final Logger logger = LoggerSupplier.forClass(DataService.class).get();

  private final RaskService raskService;

  @Autowired
  public DataService(RaskService raskService) {
    this.raskService = raskService;
  }

  public PersonRegistrationStatus getPersonRegistrationStatus(String personId) {
    final var optionalRegistrationStatus = raskService.getRegistrationStatus(personId);
    if (optionalRegistrationStatus.isEmpty()) {
      return new PersonRegistrationStatus(personId, false, null, null);
    }
    final var registrationStatus = optionalRegistrationStatus.get();
    final var jobseeker = registrationStatus.getArbetssokande();
    final var processStatusPersonnummer = jobseeker.getPersonnummer();

    if (!personId.equals(processStatusPersonnummer)) {
      logger.error("Detected data inconsistency! Received data for unexpected personal number!");
      throw new ApiException(
          ErrorResponseFactory.createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    final var processStatus = registrationStatus.getProcessStatus();
    final var sparStatus = processStatus.getSparStatus();
    final var isRegistered =
        sparStatus.equals(
            ArbetsokandeProcessStatusDTOKompositProcessStatus.SparStatusEnum.INSKRIVEN);
    final var registreringsDatumString = processStatus.getAktuellSedanDatum();
    final var avregistreringsDatumString = processStatus.getAvaktualiseringsDatum();

    final var personRegistrationStatus =
        new PersonRegistrationStatus(
            personId, isRegistered, registreringsDatumString, avregistreringsDatumString);

    warnOnIllegalCombinations(personRegistrationStatus);
    return personRegistrationStatus;
  }

  void warnOnIllegalCombinations(PersonRegistrationStatus personRegistrationStatus) {
    final var isRegistered = personRegistrationStatus.isRegistered();
    final var registrationDate = personRegistrationStatus.registrationDate();
    final var deregistrationDate = personRegistrationStatus.deregistrationDate();

    final boolean hasRegistrationDate = null != registrationDate;
    final boolean hasDeregistrationDate = null != deregistrationDate;

    if (isRegistered && hasRegistrationDate && hasDeregistrationDate)
      logWarning(true, registrationDate, deregistrationDate);
    else if (isRegistered && !hasRegistrationDate && hasDeregistrationDate)
      logWarning(true, registrationDate, deregistrationDate);
    else if (isRegistered && !hasRegistrationDate)
      logWarning(true, registrationDate, deregistrationDate);
    else if (!isRegistered && hasRegistrationDate && !hasDeregistrationDate)
      logWarning(false, registrationDate, deregistrationDate);
  }

  private void logWarning(boolean isRegistered, LocalDate registrationDate, LocalDate deregistrationDate) {
    logger.warn(
        "Unexpected combination isRegistered: {}, registrationDate: {}, deregistrationDate: {}",
        isRegistered,
        registrationDate,
        deregistrationDate);
  }

  public GetPersonRegistrationStatusResponse createJsonResponse(
      PersonRegistrationStatus personRegistrationStatus) {
    final var response =
        new GetPersonRegistrationStatusResponse(
            personRegistrationStatus.personId, personRegistrationStatus.isRegistered);
    if (null != personRegistrationStatus.registrationDate())
      response.setRegistrationDate(
          personRegistrationStatus.registrationDate().atStartOfDay(ZoneOffset.UTC));
    if (null != personRegistrationStatus.deregistrationDate())
      response.setDeregistrationDate(
          personRegistrationStatus.deregistrationDate().atStartOfDay(ZoneOffset.UTC));

    return response;
  }

  public String createTextResponse(PersonRegistrationStatus personRegistrationStatus) {
    final var result = new StringBuilder();
    if (personRegistrationStatus.isRegistered()) {
      result.append("Inskriven");
    } else {
      result.append("Ej inskriven");
    }
    if (null != personRegistrationStatus.registrationDate()) {
      result.append(", registrerad ").append(personRegistrationStatus.registrationDate());
    }
    if (null != personRegistrationStatus.deregistrationDate()) {
      result.append(", avregistrerad ").append(personRegistrationStatus.deregistrationDate());
    }

    return result.toString();
  }

  public record PersonRegistrationStatus(
      String personId,
      boolean isRegistered,
      LocalDate registrationDate,
      LocalDate deregistrationDate) {}
}
