include:
 remote: 'https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci/-/raw/v2.1.1/jobtech-ci.yml'

variables:
 CI_DEBUG_SERVICES: false
 CI_DEBUG_TRACE: false
 POSTGRES_ENABLED: false
 TEST_DISABLED: true
 DEPLOYS_DISABLED: 1
 CI_PROJECT_NAME: min-inskrivningsstatus-api
 RELEASE_TAG: $CI_COMMIT_TAG
 FEATURE_BRANCH_TAG: $BRANCH_IMAGE

### SCRIPTS

### JOBS

sources:
 stage: build
 image: maven:3.9.9-eclipse-temurin-21
 cache:
  - key:
     files:
      - pom.xml
    paths:
     - .m2/repository
 script:
  - mvn -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository generate-sources resources:resources compiler:compile
 artifacts:
  expire_in: 30 mins
  paths:
   - target/classes
   - target/generated-sources
   - target/maven-status

test-sources:
 needs: ["sources"]
 dependencies:
  - sources
 stage: build
 image: maven:3.9.9-eclipse-temurin-21
 cache:
  - key:
     files:
      - pom.xml
    paths:
     - .m2/repository
 script:
  - mvn -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository resources:testResources compiler:testCompile
 artifacts:
  expire_in: 30 mins
  paths:
   - target/generated-test-sources
   - target/test-classes

unit-test:
 needs: ["sources", "test-sources"]
 stage: build
 dependencies:
  - sources
  - test-sources
 image: maven:3.9.9-eclipse-temurin-21
 cache:
  - key:
     files:
      - pom.xml
    paths:
     - .m2/repository
 script:
  - mvn -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository surefire:test@unit

integration-test:
 needs: ["sources", "test-sources"]
 stage: build
 dependencies:
  - sources
  - test-sources
 image: maven:3.9.9-eclipse-temurin-21
 cache:
  - key:
     files:
      - pom.xml
    paths:
     - .m2/repository
 script:
  - mvn -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository surefire:test@integration

api-tests:
 stage: test
 image: docker:27.3.1-alpine3.20
 needs:
  - build
 rules:
  - when: never
 services:
  - name: docker:28.0.1-dind
  - name: $IMAGE_NAME
    alias: miapi
    variables:
     MIAPI_BASEURL: "http://miapi:8080"
     MIAPI_CLIENTAUTHKEY: "client-auth-key"
     MIAPI_RASK_BASEURL: "http://example.com"
     MIAPI_RASK_CLIENTID: "foo"
     MIAPI_RASK_CLIENTSECRET: "bar"
     MIAPI_RASK_AFSYSTEMID: "baz"
     MIAPI_RASK_AFENDUSERID: "cux"
     MIAPI_RASK_AFENVIRONMENT: "doh"
     MIAPI_RELEASED: "2025-03-03"
 variables:
  FF_NETWORK_PER_BUILD: "true" # activate container-to-container networking
 before_script:
  - apk add --no-cache curl
 script:
  - "curl -vS -H 'X-Auth-Key: client-auth-key' -H 'Accept: text/plain' 'http://miapi:8080/v0/persons/1234567890/registrationStatus'"

api-tests:featurebranch:
 extends: api-tests
 variables:
  IMAGE_NAME: $FEATURE_BRANCH_TAG
 rules:
  - if: $CI_COMMIT_TAG
    when: never
  - when: always

api-tests:release:
 extends: api-tests
 variables:
  IMAGE_NAME: $CI_REGISTRY_IMAGE:$RELEASE_TAG
 rules:
  - if: $CI_COMMIT_TAG
    when: always
  - when: never

code-coverage-test:
 needs: ["sources", "test-sources"]
 stage: build
 dependencies:
  - sources
  - test-sources
 image: maven:3.9.9-eclipse-temurin-21
 cache:
  - key:
     files:
      - pom.xml
    paths:
     - .m2/repository
 script:
  - mvn -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository jacoco:prepare-agent surefire:test jacoco:check@coverage jacoco:report
  # Print total coverage to log so it can be extracted by the coverage regex
  - 'cd target/site/jacoco/'
  - awk -F"," '{ instructions += $4 + $5; covered += $5 } END { print covered, "/", instructions, " instructions covered"; print "Coverage:", int(100*covered/instructions), "%" }' jacoco.csv
 artifacts:
  expire_in: 30 mins
  paths:
   - target/site/jacoco
 coverage: '/Coverage:.*?([0-9]{1,3}) %/'

build_jar:
 needs: ["sources"]
 stage: build
 dependencies:
  - sources
 image: maven:3.9.9-eclipse-temurin-21
 cache:
  - key:
     files:
      - pom.xml
    paths:
     - .m2/repository
 script:
  - mvn -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository jar:jar spring-boot:repackage
 artifacts:
  expire_in: 30 mins
  paths:
   - target/min-inskrivningsstatus-api-*.jar

cobertura-report:
 needs: ["code-coverage-test"]
 stage: build
 dependencies:
  - code-coverage-test
 image: registry.gitlab.com/haynes/jacoco2cobertura:1.0.10
 script:
  # convert report from jacoco to cobertura, using relative project path
  - python /opt/cover2cover.py target/site/jacoco/jacoco.xml $CI_PROJECT_DIR/src/main/java/ > target/site/cobertura.xml
 artifacts:
  expire_in: 1 week
  paths:
   - target/site/jacoco/
  reports:
   coverage_report:
    coverage_format: cobertura
    path: target/site/cobertura.xml

pages:
 needs: ["cobertura-report"]
 stage: build
 only:
  - main
 dependencies:
  - cobertura-report
 script:
  - mv target/site/jacoco/ public/
 artifacts:
  expire_in: 30 mins
  paths:
   - public/

build:
 needs: [build_jar]

build_artifact:
 needs: [build_jar]
 stage: build
 dependencies:
  - build_jar
 image: docker:28.0.1
 services:
  - docker:28.0.1-dind
 script:
  - if [ ! -f Dockerfile ]; then echo "No Dockerfile in this repo, so no build needed." >&2; exit; fi
  # login to Nexus, so we have access to the JobTech base image
  - docker login -u pipeline-readonly -p "$docker_images_jobtechdev_se_passwd" docker-images.jobtechdev.se
  # login to the project's GitLab repo
  - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
  - git_tag=""
  - git_tag_flag=""
  - if [ "$CI_COMMIT_REF_NAME" != "$CI_COMMIT_REF_SLUG" ]; then git_tag="${CI_COMMIT_REF_NAME//\//_}"; git_tag_flag="-t $CI_REGISTRY_IMAGE/$CI_PROJECT_NAME:$git_tag"; fi
  - docker build -t $TAG_COMMIT -t $TAG_LATEST -t $TAG_BRANCH $git_tag_flag .
  - docker login -u "$NEXUS_WRITE_USER" -p "$NEXUS_WRITE_PASS" docker-images.jobtechdev.se
  - for tag in latest $CI_COMMIT_SHORT_SHA $CI_COMMIT_REF_SLUG $git_tag; do
  -   docker push $CI_REGISTRY_IMAGE/$CI_PROJECT_NAME:$tag
  -   docker tag  $CI_REGISTRY_IMAGE/$CI_PROJECT_NAME:$tag docker-images.jobtechdev.se/$CI_PROJECT_NAME/$CI_PROJECT_NAME:$tag
  -   docker push docker-images.jobtechdev.se/$CI_PROJECT_NAME/$CI_PROJECT_NAME:$tag
  - done

bump-version-dry-run:
 needs: [build]
 stage: deploy
 when: manual
 image: node:22
 variables:
  GIT_DEPTH: 999
 before_script:
  - git fetch
  - git checkout $CI_COMMIT_REF_NAME
  - npm i -g commit-and-tag-version@12.2.0
  - git config --global user.name "Bump version bot"
  - git config --global user.email "bump-version-bot@jobtechdev.se"
  - git remote set-url origin "https://BUMP_VERSION_ACCESS_TOKEN:${BUMP_VERSION_ACCESS_TOKEN}@${CI_REPOSITORY_URL#*@}"
 script:
  - npx commit-and-tag-version --dry-run --packageFiles pom.xml --bumpFiles pom.xml

bump-version:
 needs: [build]
 stage: deploy
 only:
  - main
 when: manual
 image: node:22
 variables:
  GIT_DEPTH: 999
 before_script:
  - git fetch
  - git checkout $CI_COMMIT_REF_NAME
  - npm i -g commit-and-tag-version@12.2.0
  - git config --global user.name "Bump version bot"
  - git config --global user.email "bump-version-bot@jobtechdev.se"
  - git remote set-url origin "https://BUMP_VERSION_ACCESS_TOKEN:${BUMP_VERSION_ACCESS_TOKEN}@${CI_REPOSITORY_URL#*@}"
 script:
  - npx commit-and-tag-version --packageFiles pom.xml --bumpFiles pom.xml
  - git push --follow-tags origin main

bump-version-v1:
 needs: [build]
 stage: deploy
 only:
  - main
 when: manual
 image: node:22
 variables:
  GIT_DEPTH: 999
 before_script:
  - git fetch
  - git checkout $CI_COMMIT_REF_NAME
  - npm i -g commit-and-tag-version@12.2.0
  - git config --global user.name "Bump version bot"
  - git config --global user.email "bump-version-bot@jobtechdev.se"
  - git remote set-url origin "https://BUMP_VERSION_ACCESS_TOKEN:${BUMP_VERSION_ACCESS_TOKEN}@${CI_REPOSITORY_URL#*@}"
 script:
  - npx commit-and-tag-version --packageFiles pom.xml --bumpFiles pom.xml --release-as 1.0.0
  - git push --follow-tags origin main
